// @jorjun

interface Row {
    low: number
    high: number
    letter: string
    pwd: string
}

const loadData = (path: string) => Deno.readTextFileSync(path).split('\n'),
    regExp = /^(\d+)-(\d+)\s(\w): (\w+)/,
    parse = (row: string): Row => {
        const match = row.match(regExp)
        if (!match) throw `Bad record: ${row}`
        const [_low, _high, letter, pwd] = match.slice(1)
        return { low: Number(_low), high: Number(_high), letter, pwd }
    },
    isValid = ({ low, high, letter, pwd }: Row) => {
        const occur = () => pwd.split('').filter((_) => _ === letter).length,
            found = occur()
        if (found < low || found > high) return false
        return true
    }

const data = loadData('./day8/data.dat')
let count = 0
for (const _ of data.map(parse).filter((row: Row) => isValid(row))) count++
console.log(count)
