// @jorjun  V:vii ☉ ♐  ☽ ♒

interface Row {
    low: number
    high: number
    letter: string
    pwd: string
}

const loadData = (path: string) => Deno.readTextFileSync(path).split('\n'),
    regExp = /^(\d+)-(\d+)\s(\w): (\w+)/,
    parse = (row: string): Row => {
        const match = row.match(regExp)
        if (!match) throw `Bad record: ${row}`
        const [_low, _high, letter, pwd] = match.slice(1)
        return { low: Number(_low), high: Number(_high), letter, pwd }
    },
    isValid = ({ low, high, letter, pwd }: Row) => {
        const [l1, l2] = [pwd[low - 1], pwd[high - 1]]
        if (l1 !== l2 && (l1 === letter || l2 === letter)) return true
        return false
    }

const count = loadData('./day8/data.dat')
    .map(parse)
    .filter((row: Row) => isValid(row))
    .map((_) => 1)
    .reduce((prv, cur) => prv + cur)
console.log(count)
