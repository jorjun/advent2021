const loadData = (path: string) => Deno.readTextFileSync(path).split('\n').map(Number),
    data = loadData('./2020/day1/data.dat'),
    total = (target = 2020) => {
        for (let a = 0; a < data.length; a++)
            for (let b = 0; b < data.length; b++)
                for (let c = 0; c < data.length; c++) if (data[a] + data[b] + data[c] === target) return [a, b, c]
        return [-1, -1, -1]
    }

const [x, y, z] = total()
console.log(data[x], data[y], data[z], data[x] * data[y] * data[z])
