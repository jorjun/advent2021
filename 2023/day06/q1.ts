// @jorjun Vix ☉ : ♐ ☽ : ♍

interface Race {
  duration: number,
  distance: number
}

class Question {
  data: string[]
  race_list: Race[] = []
  total = 1

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n").filter(_ => _)
    const fetch = this.fetch_data()

    const [duration, distance] = [1, 2].map(
      () => this.parse_numbers(fetch.next().value)
    )
    duration.map(
      (duration, ix) => this.race_list.push({ duration, distance: distance[ix] })
    )

    this.race_list.map(race => Array.from(this.speed(race))).map(dist => {
      console.log({ len: dist.length })
      this.total *= dist.length
    })
  }

  *speed(race: Race) {
    let distance = 0
    for (let dur = 1; dur < race.duration; dur++) {
      const speed = dur
      distance = speed * (race.duration - dur)
      if (distance > race.distance)
        yield distance
    }
  }

  * fetch_data() {
    for (const line of this.data) yield line
  }

  parse_numbers(line: string) {
    return [...line.matchAll(/\d+/g).map((match) => Number(match[0]))]
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(q.total)