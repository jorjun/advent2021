// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  word_list = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
  regex = /(?=(\d|one|two|three|four|five|six|seven|eight|nine))/g
  total = 0

  constructor (private path = "./d1.txt") { }

  async load() {
    this.data = await Deno.readTextFile(this.path)

    for (const line of this.data.split("\n").filter(_ => _)) {
      const num = this.parse_line(line)
      this.total += Number(num.join(''))
    }
  }

  parse_line(line: string) {
    const digit_or_word = Array.from(line.matchAll(this.regex), (match) => match[1])
    return [digit_or_word[0], digit_or_word.at(-1)].map(item => {
      const num = Number(item)
      if (num) return num
      return this.word_list.indexOf(item) + 1
    })
  }

}

// const q = new Question("./t2.txt")
const q = new Question()
await q.load()
console.log(
  q.total
)
