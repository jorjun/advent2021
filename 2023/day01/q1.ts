// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  total = 0

  constructor (private path = "./d1.txt") { }

  async load() {
    this.data = await Deno.readTextFile(this.path).catch((err) =>
      console.error(err)
    )

    for (const line of this.data.split("\n")) {
      const sum = this.parse_line(line)
      console.log(sum)
      this.total += sum
    }
  }

  parse_line(line: string) {
    const numbers = line.split('').map(ch => Number(ch)).filter(num => num)
    let result: number[] = []
    for (const num of numbers) {
      result.push(num)
      break
    }

    for (const num of numbers.reverse()) {
      result.push(num)
      break
    }

    if (result.length < 2) {
      result.push(result[0])
    }
    return Number(result.join(''))
  }

  output() {
    console.log(this.total)
  }
}

// const q = new Question("./t1.txt")
const q = new Question()
await q.load()
console.log(
  q.output()
)
