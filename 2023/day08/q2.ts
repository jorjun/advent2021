// @jorjun Vix ☉ : ♐ ☽ : ♍

function gcd(a: number, b: number): number {
  while (b > 0) [a, b] = [b, a % b]
  return a
}

const lcm = (array: number[]): number => array.reduce((prv, cur) => prv *= cur / gcd(cur, prv), 1)
const parse_row = (row: string) => [.../(\w+) = \((\w+), (\w+)/.exec(row).slice(1)]

class Question {
  data: string[]
  instruct: string[]

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const data = _data.split("\n").filter(_ => _)
    this.instruct = data[0].split('')
    const map = {}
    data.slice(1).forEach(row => {
      const [start, left, right] = parse_row(row)
      map[start] = [left, right]
    })
    let current_node = Object.keys(map).filter(_ => _.endsWith('A')),
      steps = Array(current_node.length).fill(0), count = Array(current_node.length).fill(0)
    console.log({ current_node })
    for (const instr of this.get_instruct()) {
      current_node = current_node.map((node, ix) => {
        steps[ix] += 1
        const next = map[node][instr === 'L' ? 0 : 1]
        if (next[2] === 'Z') {
          if (count[ix] === 0) count[ix] = steps[ix] // Steps stored once
          if (count[ix] !== steps[ix]) throw 'no cycle' // Are they always the same, i.e. a cycle?
          console.log({ ix, s: steps[ix], c: count[ix] })
          steps[ix] = 0
        }
        return next
      })
      if (count.filter(_ => _ > 0).length === count.length) break // Counts for every node saved
    }
    console.log({ current_node, count, lcm: lcm(count) })
  }

  * get_instruct() {
    while (1) for (const instr of this.instruct) yield instr
  }


}

// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
