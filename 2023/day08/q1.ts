// @jorjun Vix ☉ : ♐ ☽ : ♍


class Question {
  data: string[]

  instruct: string[]

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const data = _data.split("\n").filter(_ => _)
    this.instruct = data[0].split('')
    const map = {}
    const node_map = data.slice(1).map(row => {
      const [start, left, right] = this.parse_row(row)
      map[start] = [left, right]
    })
    let current_node = Object.keys(this.node_map).filter(_ => _.endsWith('A')), steps = 0
    console.log({ current_node })
    for (const instr of this.get_instruct()) {
      steps++
      current_node = map[current_node][instr === 'L' ? 0 : 1]
      if (current_node === "ZZZ") break
      if (steps % 100_000_000 === 0) console.log({ steps, current_node })
    }
    console.log({ current_node, steps })
  }

  * get_instruct() {
    while (1)
      for (const instr of this.instruct) {
        yield instr
      }
  }

  parse_row(row: string) {
    return [.../(\w+) = \((\w+), (\w+)/.exec(row).slice(1)]
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
