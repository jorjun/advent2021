// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Card {
  win: number[]
  mine: number[]
}

class Question {
  data: string[]
  total = 0

  constructor (private path: string) { }

  async load() {
    const data = await Deno.readTextFile(this.path)
    this.data = data.split("\n").filter(_ => _)

    const card_list: Card[] = this.data.map((line) => {
      const [win, card] = line.split('|').map(row => this.parse_numbers(row))
      return { win: win.slice(1), card }
    })

    for (const { win, card, game } of card_list) {
      const matches = card.filter(num => win.includes(num))
      if (matches.length > 0) this.total += 2 ** (matches.length - 1)
    }

  }

  parse_numbers(line: string) {
    return [...line.matchAll(/\d+/g).map((match) => Number(match[0]))]
  }

}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(
  q.total
)
