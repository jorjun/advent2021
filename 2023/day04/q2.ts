// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Card {
  [id: number]: {
    win: number[]
    lotto: number[]
    score: number
  }
}

class Question {
  data: string[]
  total = 0
  card_collect: Card = {}
  card_record: { [id: number]: [count: number] } = {}
  total = 0

  constructor (private path: string) { }

  async load() {
    const data = await Deno.readTextFile(this.path)
    this.data = data.split("\n").filter(_ => _)

    this.data.forEach((line) => {
      const [win, lotto] = line.split('|').map(row => this.parse_numbers(row))
      const id = Number(win[0])
      this.card_collect[id] = { win: win.slice(1), lotto, score: -1 }
      this.card_record[id] = 1
    })

    this.draw(Object.keys(this.card_record).map(_ => Number(_)))
    console.log(this.card_record)
    this.total = Object.values(this.card_record).reduce((prv, cur) => prv + cur, 0)

  }

  draw(id_list: number[]) {
    for (const id of id_list) {
      const score = this.play(id)
      if (score === 0) continue
      for (let iy = 0; iy < this.card_record[id]; iy++)
        for (let ix = id + 1; ix <= score + id; ix++) this.card_record[ix] += 1
    }

  }

  play(id: number) {
    let { score, lotto, win } = this.card_collect[id]
    if (score === -1) {
      // cache score
      score = lotto.filter(num => win.includes(num)).length
      this.card_collect[id].score = score
    }
    return score
  }

  parse_numbers(line: string) {
    return [...line.matchAll(/\d+/g).map((match) => Number(match[0]))]
  }

}


const q = new Question("./test.txt")
// const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(q.total)
