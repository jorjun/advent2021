// @jorjun Vix ☉ : ♐ ☽ : ♍

const STRENGTH: Record<[hand_type: string], number> = {
  '5': 7,
  '41': 6,
  '32': 5,
  '311': 4,
  '221': 3,
  '2111': 2,
  '11111': 1
}
const ORDER = '23456789TJQKA'

class Question {
  data: string[]
  race_list: Race[] = []
  total = 1

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const deal = _data.split("\n").filter(_ => _)
      .map(line => line.split(' '))
      .map(([cards, bid]) => ({ cards, bid: Number(bid) }))
    const analyze = deal.map(({ cards, bid }) => {
      const strength = this.get_strength(cards)
      return {
        cards,
        bid,
        strength,
        order: cards.split('').map((ch) => ORDER.indexOf(ch).toString(16)).join('')
      }
    })
    const a1 = analyze.sort((a, b) => `${a.strength}_${a.order}` >= `${b.strength}_${b.order}` ? 1 : -1)
      .map(({ order, cards, bid }, ix) => {
        const rnk = ix + 1
        return { order, cards, rnk, score: rnk * bid }
      })
    console.log(a1, a1.map(rec => rec.score).reduce((prv, sum) => prv + sum), 0)
  }

  get_strength(cards: string) {
    const frequency: Record<[hand_type: string], [count: number]> = {}
    cards.split('').forEach(ch => {
      if (frequency[ch] == undefined) frequency[ch] = 0
      frequency[ch] += 1
    })
    const hand_type = Object.values(frequency).sort().reverse().join('')
    return STRENGTH[hand_type]
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
