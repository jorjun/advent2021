// @jorjun Vix ☉ : ♐ ☽ : ♍

const STRENGTH: Record<[hand_type: string], number> = {
  '5': 7,
  '41': 6,
  '32': 5,
  '311': 4,
  '221': 3,
  '2111': 2,
  '211': 4,
  '11111': 1,
  '4': 7,
  '31': 6,
  '22': 5,
  '1111': 2,
  '3': 7,
  '21': 6,
  '111': 4,
  '2': 7,
  '11': 6,
  '1': 7,
  '': 7,
}
const ORDER = 'J23456789TQKA'

class Question {
  data: string[]
  race_list: Race[] = []
  total = 1

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const deal = _data.split("\n").filter(_ => _)
      .map(line => line.split(' '))
      .map(([cards, bid]) => ({ cards, bid: Number(bid) }))
    const analyze = deal.map(({ cards, bid }) => {
      const lst = cards.split('')
      // const joker_num = lst.filter(ch => ch === "J").length
      // let strength = this.get_strength(cards)
      // if (joker_num > 0 && joker_num < 4 && strength < 7) {
      //   strength = JOKER[joker_num - 1][strength]
      // }
      // if (joker_num > 3) strength = 7
      const res = {
        cards,
        bid,
        strength: this.get_strength(cards),
        order: lst.map((ch) => ORDER.indexOf(ch).toString(16)).join('')
      }
      // if (cards.indexOf('J') > -1) console.log({ res })
      return res
    })
    const a1 = analyze.sort((a, b) => `${a.strength}_${a.order}` >= `${b.strength}_${b.order}` ? 1 : -1)
      .map(({ strength, order, cards, bid, uniq }, ix) => {
        const rnk = ix + 1
        return { strength, cards, score: rnk * bid, rnk }
      })
    a1.forEach(row => console.log(row))
    console.log(a1.map(rec => rec.score).reduce((prv, sum) => prv + sum), 0)
  }

  get_strength(cards: string) {
    const frequency: Record<[hand_type: string], [count: number]> = {}
    cards.split('').filter(ch => ch != 'J').forEach(ch => frequency[ch] = frequency[ch] + 1 || 1)
    const hand_type = Object.values(frequency).sort().reverse().join('')
    const strength = STRENGTH[hand_type]
    if (strength === undefined) console.log({ hand_type })
    return strength
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
