// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  line_reg = /^Game (\d+): (.*)/
  total = 0

  constructor (private path = "./d1.txt") { }

  async load() {
    this.data = await Deno.readTextFile(this.path)

    for (const line of this.data.split("\n").filter(_ => _)) {
      const { red, green, blue } = this.parse_line(line)
      this.total += red * blue * green
    }
  }

  parse_line(line: string) {
    const result = { red: 0, blue: 0, green: 0 }
    const [_game, bag] = Array.from(line.match(this.line_reg)).slice(1)
    bag.split(';')
      .map(row => row.split(',')
        .map(num_color => {
          const [_num, color] = num_color.trim().split(' '), num = Number(_num)
          if (result[color] < num) result[color] = num
        })
      )
    return result
  }
}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.load()
console.log(
  q.total
)
