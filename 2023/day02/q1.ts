// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  colour_map = { red: 12, blue: 14, green: 13 }
  total = 0
  line_reg = /^Game (\d+): (.*)/
  fetch_reg = /(\d+) (\w+),/g

  constructor (private path = "./d1.txt") { }

  async load() {
    this.data = await Deno.readTextFile(this.path)

    for (const line of this.data.split("\n").filter(_ => _)) {
      const { game, result } = this.parse_line(line)
      if (result.length == 0)
        this.total += Number(game)
    }
  }

  parse_line(line: string) {
    const [game, bag] = Array.from(line.match(this.line_reg)).slice(1)
    const result = []
    const fetched = bag.split(';')
      .map(row => row.split(',').map(_ => {
        const [num, color] = _.trim().split(' ')
        const row = { color, num: Number(num) }
        const ok = this.colour_map[color] >= row.num
        result.push({ ...row, ok })
      }))
    return { game, result: result.filter(row => !row.ok) }
  }
}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.load()
console.log(
  q.total
)
