// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Part {
  num: number
  ix: number
  len: number
}

interface Symbol {
  sym: string
  ix: number
}

class Question {
  data: string[]
  part_list: Part[] = []
  symbol_map: Record<number, Symbol[]> = {}
  total = 0

  constructor (private path: string) { }

  async load() {
    const data = await Deno.readTextFile(this.path)
    this.data = data.split("\n").filter(_ => _)

    this.part_list = this.data.map((line, iy) => this.parse_numbers(line, iy)).flat()
    this.data.map((line, iy) => {
      const lst = this.parse_symbols(line, iy).flat()
      if (lst.length > 0)
        this.symbol_map[iy] = lst
    })
    for (const part of this.part_list) {
      this.total += this.is_part(part)
    }
  }

  parse_numbers(line: string, iy: number) {
    return [
      ...line.matchAll(/(\d+)/g)
        .map((match) =>
          ({ iy, ix: match.index, num: Number(match[1]), len: match[1].length }))
    ]
  }

  parse_symbols(line: string) {
    return [
      ...line.matchAll(/([^\d\.])/g)
        .map((match) =>
          ({ ix: match.index, sym: match[0] }))
    ]
  }

  is_part(part: Part) {
    const iy = part.iy
    const symbol_list = [iy - 1, iy, iy + 1].filter(y => y >= 0)
      .map(y => this.symbol_map[y])
      .filter(_ => _)
      .flat()
    for (const symbol of symbol_list) {
      if (symbol.ix > part.ix - 2 && symbol.ix < part.ix + part.len + 1) {
        console.log({ iy: iy + 1, num: part.num, symbol: symbol.sym })
        return part.num
      }
    }
    return 0
  }

}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(
  q.total
)
