// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Part {
  num: number
  ix: number
  len: number
}

interface Symbol {
  sym: string
  ix: number
  iy: number
}

class Question {
  data: string[]
  part_list: Part[] = []
  symbol_map: Record<[iy: number], Symbol[]> = {}
  total = 0

  constructor (private path: string) { }

  async load() {
    const data = await Deno.readTextFile(this.path)
    this.data = data.split("\n").filter(_ => _)

    this.part_list = this.data.map((line, iy) => this.parse_numbers(line, iy)).flat()

    // this.symbol_map
    this.data.forEach((line, iy) => {
      const lst = this.parse_symbols(line, iy)
      if (lst.length > 0)
        this.symbol_map[iy] = lst
    })

    const gear: Record<string, Part[]> = {}
    this.part_list.map(p => this.get_part(p))
      .filter(({ num }) => num > 0)
      .map(part => {
        const key = `${part.ix}:${part.iy}`
        if (gear[key] == undefined) gear[key] = [part]
        else gear[key].push(part)
      })

    this.total = Object.values(gear)
      .filter(_ => _.length == 2)
      .map(([{ num: a }, { num: b }]) => a * b)
      .reduce((prv, cur) => prv + cur, 0)
  }

  parse_numbers(line: string, iy: number) {
    return [
      ...line.matchAll(/(\d+)/g)
        .map((match) => {
          const { index: ix } = match, { length: len } = match[0], num = Number(match[0])
          return { ix, iy, num, len }
        })
    ]
  }

  parse_symbols(line: string, iy: number) {
    return [
      ...line.matchAll(/(\*)/g)
        .map((match) =>
          ({ iy, ix: match.index, sym: match[0] }))
    ]
  }

  get_part(part: Part) {
    const { ix: part_ix, iy: part_iy, num, len } = part
    const symbol_candidates = [part_iy - 1, part_iy, part_iy + 1]
      .filter(_y => _y >= 0) // Valid Y
      .map(_y => this.symbol_map[_y]) // symbols are indexed by Y-coordinate
      .filter(_ => _) // at least one symbol
      .flat()

    for (const { ix, iy } of symbol_candidates)
      if (ix > part_ix - 2 && ix < part_ix + len + 1) return { num, ix, iy }

    return { num: 0 }
  }

}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(q.total)
