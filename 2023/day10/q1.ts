// @jorjun Vix ☉ : ♐ ☽ : ♏

const Symbol: Record<string, [nsew: number[]]> = {
  "|": "vertical",
  "-": "horizontal",
  "J": "clockwise",
  "L": "anticlockwise",
  "7": "anticlockwise",
  "F": "clockwise",
}

class Question {
  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const data = _data.split("\n").filter(_ => _)
    const grid: Record<number, number[][]> = []
    let start
    data.forEach((row, ix) => {
      grid[ix] = data[ix].split('')
      const pos = grid[ix].indexOf("S")
      console.log({ pos })
      if (pos > -1) start = [ix, pos]
    })
    console.log({ grid, start })

  }
}

const q = new Question("./test.txt")
// const q = new Question("./data.txt")
console.log(new Date())
await q.load()
