// @jorjun Vix ☉ : ♐ ☽ : ♍

interface Almanac {
  dst: number
  src: number,
  len: number
}

class Question {
  data: string[]
  map: { [key: string]: Almanac[] } = {}

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n")
    const data_gen = this.fetch_data()
    const seeds = this.parse_numbers(data_gen.next().value)
    const _blank_line = data_gen.next()

    // Category parse
    for (let ix = 1; ix <= 10; ix++) {
      const { value: line, done } = data_gen.next()
      if (done || line === "") break // No more categories

      const [frm, _] = this.parse_category(line)
      // parse range number rows for this category
      while (1) {
        const { value: number, done } = data_gen.next()
        if (done || number == "") break

        const [dst, src, len] = this.parse_numbers(number)
        if (this.map[frm] == undefined) this.map[frm] = [{ dst, src, len }]
        else this.map[frm].push({ dst, src, len })
      }
    }

    const min = seeds.map(seed => this.mapped(seed))
    console.log("Min:", min.sort().at(-1))
  }

  mapped(inp: number) {
    let dest = inp
    for (const key of Object.keys(this.map)) {
      for (const { dst, src, len } of this.map[key]) {
        const max = src + len - 1
        if (dest >= src && dest <= max) {
          dest = dest - src + dst
          break
        }
      }
    }
    return dest
  }

  * fetch_data() {
    for (const line of this.data) yield line
  }

  parse_numbers(line: string) {
    return [...line.matchAll(/\d+/g).map((match) => Number(match[0]))]
  }

  parse_category(line: string) {
    return /^(\w+)-to-(\w+) map:$/.exec(line).slice(1)
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log()