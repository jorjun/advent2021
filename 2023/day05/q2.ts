// @jorjun Vix ☉ : ♐ ☽ : ♍

interface Almanac {
  dst: number
  src: number,
  len: number
}

class Question {
  data: string[]
  map: { [key: number]: Almanac[] } = {}
  seed_row: number[]

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n")
    const data_gen = this.fetch_data()
    this.seed_row = this.parse_numbers(data_gen.next().value)
    const _blank_line = data_gen.next()

    // Category parse
    for (let ix = 0; ix <= 7; ix++) {
      const { value: line, done } = data_gen.next()
      if (done || line === "") break // No more categories

      const [frm, _] = this.parse_category(line)
      // parse range number rows for this category
      while (1) {
        const { value: number, done } = data_gen.next()
        if (done || number == "") break

        const [dst, src, len] = this.parse_numbers(number)
        if (this.map[ix] == undefined) this.map[ix] = [{ dst, src, len }]
        else this.map[ix].push({ dst, src, len })
      }
    }

    let min = Number.MAX_SAFE_INTEGER
    for (const seed of this.fetch_seed()) {
      const loc = this.mapped(seed)
      if (loc < min) min = loc
    }
    console.log({ min })
  }

  mapped(inp: number) {
    let dest = inp
    for (let ix = 0; ix <= 6; ix++) {
      for (const { dst, src, len } of this.map[ix]) {
        const max = src + len - 1
        if (dest >= src && dest <= max) {
          dest = dest - src + dst
          break
        }
      }
    }
    return dest
  }


  * fetch_seed() {
    const batch = []
    const { seed_row } = this
    for (let ix = 0; ix < seed_row.length; ix += 2) {
      const [start, count] = [seed_row[ix], seed_row[ix + 1]]
      for (let iy = start; iy < start + count; iy++) yield iy
    }
  }

  * fetch_data() {
    for (const line of this.data) yield line
  }

  parse_numbers(line: string) {
    return [...line.matchAll(/\d+/g).map((match) => Number(match[0]))]
  }

  parse_category(line: string) {
    return /^(\w+)-to-(\w+) map:$/.exec(line).slice(1)
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log()