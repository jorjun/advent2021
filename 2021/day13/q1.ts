import { loadData, foldList, doFold, render } from './lib.ts'

loadData()
console.log(foldList)
render()
for (const { axis, val } of foldList) {
    doFold(axis, val)
    break
}
render()
