// @jorjun  V:vii ☉ ♐  ☽ ♉
import { loadData, foldList, doFold, render } from './lib.ts'

loadData()
for (const { axis, val } of foldList) doFold(axis, val)
render()
