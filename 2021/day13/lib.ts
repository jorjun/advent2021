// Day 13 Advent of Code 2021  @jorjun V: vii ☉ ♐  ☽ ♉

/** I am too far behind (today is day 16) to abstract the solution into classes to reduce noisy namespace clutter and layer everything semantically.
  Can't resist combing and tidying tho' :D  
  Whether you can take pride in your work or you are merely the tactical slave utensil (TSU) of a politico-pension borg (PPB) is your own choice to make.
*/

export type Fold = { axis: string; val: number }
export type Point = [x: number, y: number]

export const DATA = './day13/data.dat',
    _raw = Deno.readTextFileSync(DATA).split('\n'),
    foldList: Fold[] = [],
    max = { x: 0, y: 0 },
    render = () => {
        let count = 0
        for (const row of grid.slice(0, max.y)) {
            const cols = row.slice(0, max.x)
            console.log(cols.join(''))
            count += cols.filter((_) => _ === '#').length
        }
        console.log(`-------${count}---------------------`)
    }

let grid: string[][] = []

function* gridIter() {
    for (let y = 0; y < max.y; y++) for (let x = 0; x < max.x; x++) yield { x, y, cell: grid[y][x] }
}

export function doFold(axis: Fold['axis'], val: number) {
    // to 'mirror' a => a' formula is: a' = axis - (axis-a)  ==> 2 x axis -a
    for (const { x, y, cell } of gridIter()) {
        if (cell !== '#') continue // only consider points
        if (axis === 'y' && y >= val) grid[2 * val - y][x] = cell
        if (axis === 'x' && x >= val) grid[y][2 * val - x] = cell
    }
    if (axis === 'y') max.y = val
    if (axis === 'x') max.x = val
}

export function loadData() {
    const points: Point[] = []

    for (const row of _raw) {
        const _blankRow = row === ''
        if (_blankRow) continue
        // parse rows into grid position or fold instruction
        const _foldInstruction = row.indexOf('fold') === 0
        if (_foldInstruction) {
            const [axis, val] = row.split('fold along ')[1].split('=')
            foldList.push({ axis, val: Number(val) })
        } else {
            const [x, y] = row.split(',').map(Number)
            points.push([x, y])
            const bigger = (v: number, maxD: 'x' | 'y') => Math.max(v, max[maxD])
            ;[max.x, max.y] = [bigger(x + 1, 'x'), bigger(y + 1, 'y')]
        }
    }
    grid = [
        ...Array(max.y)
            .fill(null)
            .map((_) => Array(max.x).fill('.')),
    ]
    for (const [x, y] of points) grid[y][x] = '#'
}
