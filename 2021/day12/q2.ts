// @jorjun

type Cave = { [label: string]: string[] }
type Visited = { [label: string]: number }
type Memo = { [key: string]: string[] }

const DATA = './day12/data.dat',
    _raw = Deno.readTextFileSync(DATA).split('\n'),
    rows = _raw.map((row) => row.split('-')),
    smallCave = (label: string) => label.toLowerCase() === label,
    map: Cave = {},
    visitOK = (label: string, visited: Visited) => {
        if (smallCave(label)) return (visited[label] ?? 0) < 2
        return true
    }

function* explore(path: string[] = [], visited: Visited = {}): Generator<string[]> {
    const tooMany = Object.values(visited).filter((it) => it > 1).length > 1
    if (tooMany) return
    const cave = path[path.length - 1]
    if (cave === 'end') yield path

    if (smallCave(cave)) visited[cave] = (visited[cave] ?? 0) + 1
    const connected = map[cave].filter((it) => it !== 'start' && visitOK(it, visited)) ?? []
    for (const node of connected) yield* explore([...path, node], { ...visited })
}

rows.forEach(([label1, label2]) => {
    if (label1 in map) map[label1].push(label2)
    else map[label1] = [label2]
    if (label1 !== 'start')
        if (label2 in map) map[label2].push(label1)
        else map[label2] = [label1]
})
map['end'] = []

let count = 0
for (const _ of explore(['start'])) {
    ++count
}
console.log(count)
