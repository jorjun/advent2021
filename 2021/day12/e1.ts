// @jorjun

type Cave = { [label: string]: string[] }
type Visited = { [label: string]: boolean }

const DATA = './day12/data.dat',
    _raw = Deno.readTextFileSync(DATA).split('\n'),
    rows = _raw.map((row) => row.split('-')),
    smallCave = (word: string) => word.toLowerCase() === word,
    map: Cave = {}

function* explore(path: string[] = [], visited: Visited = {}): Generator<string[]> {
    const cave = path[path.length - 1]
    if (cave === 'end') yield path
    const connected = map[cave].filter((it) => !visited[it]) ?? []
    for (const node of connected) yield* explore([...path, node], { ...visited, ...{ [cave]: smallCave(cave) } })
}

rows.forEach(([label1, label2]) => {
    if (label1 in map) map[label1].push(label2)
    else map[label1] = [label2]
    if (label2 in map) map[label2].push(label1)
    else map[label2] = [label1]
})

let count = 0
for (const path of explore(['start'])) console.log(path, ++count)
