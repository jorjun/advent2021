const PATH = './day12/preview_data.dat',
    _raw = Deno.readTextFileSync(PATH).split('\n'),
    rows = _raw.map((row) => row.split('-'))

type Cave = Node | null | undefined
interface Visit {
    [label: string]: string
}

class Node {
    pathList: Node[] = []

    constructor(public parent: Cave, public label: string, public small: boolean) {}

    find(label = 'start'): Cave {
        if (this.label === label) return this
        let cave: Cave
        for (const node of this.pathList) {
            cave = node.find(label)
            if (cave) break
        }
        return cave
    }

    insert(label: string) {
        const small = label.toLowerCase() === label
        const cave = new Node(this, label, small)
        this.pathList.push(cave)
    }

    *cyclePaths() {
        let stop = false
        while (this.pathList.length > 0)
            for (const node of this.pathList) {
                if (this.label !== 'end') stop = true
                if (node.small && visited[node.label]) continue
                yield node
            }
    }

    findX(label = 'start', path: string[] = []) {
        // let cave: Cave = null
        if (this.label === label) {
            console.log(path)
            path = []
        }
        path.push(this.label)
        for (const _ of this.pathList)
            for (const node of this.pathList) {
                node.findX(label, path)
            }
    }

    routes(paths: string[] = []) {
        if (this.label === 'end') {
            visited = {}
            console.log(paths)
            paths = []
            return
        }
        paths.push(this.label)
        if (this.small) visited[this.label] = this.label
        for (const node of this.cyclePaths()) {
            if (node.small && this.small) break // Don't get trapped
            node.routes(paths)
        }
    }
}

function loadTree() {
    const root = new Node(null, 'start', true)
    for (const [from, to] of rows) {
        const parent = root.find(from) ?? root
        parent.insert(to)
    }
    return root
}

//-------------------------------------------------->
const root = loadTree()
let visited: Visit = {}
// root.find('end')
root.findX('end')
