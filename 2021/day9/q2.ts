// @jorjun  V:vii ☉ ♐  ☽ ♓

type Point = [number, number]
type Basin = { [key: string]: number }

const _raw = Deno.readTextFileSync('./day9/data.dat').split('\n'),
    rows = _raw.map((row) => `9${row}9`).map((row) => row.split('').map(Number)),
    rowLen = rows[0].length

function* getGrid() {
    const padRow = new Array<number>(rowLen).fill(9)
    yield padRow
    for (const row of rows) yield row
    yield padRow
}

const grid = Array.from(getGrid())
function getLowPoints() {
    const low: Point[] = []
    for (let i = 1; i < grid.length - 1; i++) {
        for (let j = 1; j < rowLen - 1; j++) {
            const c = grid[i][j],
                neighbours = [grid[i][j - 1], grid[i][j + 1], grid[i - 1][j], grid[i + 1][j]]
            if (neighbours.filter((_) => c < _).length === 4) low.push([i, j])
        }
    }
    return low
}

const lowPoints = getLowPoints(),
    basin: { [loc: number]: number } = {}

const gridCopy = grid.slice()
function mapBasin([i, j]: Point, ix: number) {
    const cell = gridCopy[i][j]
    if (cell === 9 || cell === -1) return
    gridCopy[i][j] = -1
    mapBasin([i, j - 1], ix)
    mapBasin([i, j + 1], ix)
    mapBasin([i + 1, j], ix)
    mapBasin([i - 1, j], ix)
    if (!(ix in basin)) basin[ix] = 0
    basin[ix] += 1
}

let ix = 0
for (const cell of lowPoints) mapBasin(cell, ix++)

console.table(basin)
const sorted = Object.values(basin)
    .sort((a, b) => a - b)
    .reverse()
console.log(sorted, sorted[0] * sorted[1] * sorted[2])
