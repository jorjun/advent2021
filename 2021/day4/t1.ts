import { assertEquals } from 'https://deno.land/std@0.100.0/testing/asserts.ts'

// Simple name and function, compact form, but not configurable

Deno.test({
    name: 'Grid rows',
    fn: () => {
        const x = 1 + 2
        assertEquals(x, 3)
    },
})
