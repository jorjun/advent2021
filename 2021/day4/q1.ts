// @jorjun

type Grid = number[][]
enum Dir {
    column,
    row,
}

class Card {
    marked: boolean[][] = []

    constructor(public grid: Grid) {
        for (let ix = 0; ix < 5; ix++) this.marked[ix] = new Array<boolean>(5).fill(false)
    }

    static play(cardList: Card[], drawNum: number) {
        for (let card of cardList) if (card.draw(drawNum)) return card
    }

    *cellIter() {
        for (let ix = 0; ix < 5; ix++) for (let cell of this.iter(Dir.row, ix, false)) yield cell
    }

    *iter(direction: Dir, ii: number, isMarked: boolean) {
        const { grid, marked } = this
        for (let idx = 0; idx < 5; idx++) {
            const iy = direction === Dir.row ? ii : idx,
                ix = direction === Dir.row ? idx : ii,
                num = grid[iy][ix],
                mark = marked[iy][ix]
            if (mark === isMarked) yield { ix, iy, num, mark }
        }
    }

    draw(drawNum: Number): boolean {
        for (let { iy, ix, num } of this.cellIter()) if (num === drawNum) this.marked[iy][ix] = true
        // Immediately check for a win
        for (let ii = 0; ii < 5; ii++) {
            const rows = Array.from(this.iter(Dir.row, ii, true))
            if (rows.length === 5) return true
            const cols = Array.from(this.iter(Dir.column, ii, true))
            if (cols.length === 5) return true
        }
        return false
    }

    static *parseGrids(rows: string[]) {
        let grid: number[][] = []
        for (let ix = 0; ix < rows.length; ix++) {
            const numbers = rows[ix].split(' ').filter((numb) => numb !== '')
            if (numbers.length === 0) {
                yield grid
                grid = []
            } else grid.push(numbers.map(Number))
        }
        yield grid // final grid
    }
}

// Start ------------------------------------------------------------>

const raw = await Deno.readTextFile('day4/data.dat'),
    rows = raw.split('\n'),
    drawList = rows[0].split(',').map(Number),
    cardRaw = rows.slice(2),
    cardList: Card[] = []

for (let grid of Card.parseGrids(cardRaw)) {
    let card = new Card(grid)
    cardList.push(card)
}

for (let drawNum of drawList) {
    const card = Card.play(cardList, drawNum)
    if (card) {
        const unmarked = Array.from(card.cellIter()).map((cell) => cell.num)
        console.table(unmarked.reduce((prv, cur) => cur + prv, 0) * drawNum)
        break
    }
}
