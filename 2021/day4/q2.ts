// @jorjun V:vii ☉ ♐  ☽ ♐

type Grid = number[][]
enum Dir {
    column,
    row,
}

class Bingo {
    static cardList: Bingo[] = []
    static drawList: number[] = []
    marked: boolean[][] = []
    hasWon = false

    constructor(public grid: Grid) {
        for (let ix = 0; ix < 5; ix++) this.marked[ix] = new Array<boolean>(5).fill(false)
    }

    static *draw(drawNum: number) {
        for (let card of Bingo.cardList.filter(({ hasWon }) => !hasWon)) {
            for (let { iy, ix, num } of card.cellIter()) if (num === drawNum) card.marked[iy][ix] = true
            // Check for a win
            for (let ii = 0; ii < 5; ii++) {
                const rows = Array.from(card.iter(Dir.row, ii, true)),
                    cols = Array.from(card.iter(Dir.column, ii, true))
                if (rows.length === 5 || cols.length === 5) {
                    card.hasWon = true
                    yield card
                }
            }
        }
    }

    *cellIter() {
        for (let ix = 0; ix < 5; ix++) for (const cell of this.iter(Dir.row, ix, false)) yield cell
    }

    *iter(direction: Dir, ii: number, isMarked: boolean) {
        const { grid, marked } = this
        for (let idx = 0; idx < 5; idx++) {
            const iy = direction === Dir.row ? ii : idx,
                ix = direction === Dir.row ? idx : ii
            if (marked[iy][ix] === isMarked) yield { ix, iy, num: grid[iy][ix] }
        }
    }

    static async loadData(path: string) {
        function* parseGrid(rows: string[]) {
            let grid: number[][] = []
            for (let ix = 0; ix < rows.length; ix++) {
                const numbers = rows[ix].split(' ').filter((numb) => numb !== '')
                if (numbers.length === 0) {
                    yield grid // card separator row encountered
                    grid = [] // new card data
                } else grid.push(numbers.map(Number))
            }
            yield grid // Final grid has no separator
        }

        const _buff = await Deno.readTextFile(path),
            rows = _buff.split('\n'),
            cardRaw = rows.slice(2)
        Bingo.drawList = rows[0].split(',').map(Number)
        for (const grid of parseGrid(cardRaw)) Bingo.cardList.push(new Bingo(grid))
    }
}

// Start ------------------------------------------------------------>
await Bingo.loadData('./day4/data.dat')

let card: Bingo | null = null,
    drawNum = 0
for (const num of Bingo.drawList) {
    const wins: Bingo[] = Array.from(Bingo.draw(num)).reverse()
    if (wins.length > 0) {
        card = wins[0]
        drawNum = num
    }
}

if (!card) throw 'No winners!'
const unmarked = Array.from(card.cellIter()).map((cell) => cell.num),
    sum = unmarked.reduce((prv, cur) => cur + prv, 0)
console.table(sum * drawNum)
