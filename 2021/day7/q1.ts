// @jorjun V:vii ☉ ♐  ☽ ♑

const loadData = (path: string) => Deno.readTextFileSync(path).split(',').map(Number),
    crabX = loadData('./day7/data.dat'),
    cost = (num: number) => (num * (num + 1)) / 2,
    fuel = (b: number) => crabX.reduce((prv, a) => prv + cost(Math.abs(a - b)))

let best = Infinity,
    used = 0

for (let ix = 0; ix < crabX.length; ix++) if ((used = fuel(ix)) < best) best = used

console.log(best)
