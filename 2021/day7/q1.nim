# @jorjun V:vii ☉ ♐  ☽ ♑

import
  strutils,
  sequtils,
  sugar

const
  loadData = (path: string) => readFile(path).split(",").mapIt strip(it).parseInt
  crabX = loadData("./day7/data.dat")
  cost = (num: int) => (num * (num + 1)) div 2
  fuel = (b: int) => crabX.mapIt(cost(abs(it - b))).foldl(a+b)

var
  best = 0
  used = 0

for ix, _ in crabX:
  used = fuel(ix)
  best = if used < best or ix == 0: used else: best

echo best
