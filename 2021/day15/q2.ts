// @jorjun

type Cell = { x: number; y: number }

const DATA = './day15/preview_data.dat',
    WID = 5,
    grid = Deno.readTextFileSync(DATA)
        .split('\n')
        .map((row) => row.split('').map(Number)),
    [maxY, maxX] = [grid.length, grid[0].length],
    totalRisk: number[][] = Array(grid.length * WID)
        .fill(null)
        .map((_) => Array(grid[0].length * WID).fill(Infinity)),
    proximity = ({ x, y }: Cell) =>
        [
            [0, 1],
            [1, 0],
        ]
            .map(([dx, dy]) => ({ x: x + dx, y: y + dy }))
            .filter(({ x, y }) => Math.min(x, y) >= 0 && x < maxX * WID && y < maxY * WID), // Grid bounds
    lowest = (queue: Cell[] = []) => {
        while (queue.length) {
            const { x, y } = queue.shift()!,
                [riskIX, riskIY] = [Math.floor(x / maxX), Math.floor(y / maxY)],
                [gy, gx] = [y % maxY, x % maxX]
            let oRisk = grid[gy][gx] + riskIX + riskIY
            if (oRisk > 9) oRisk -= 9
            const newRisk = oRisk + totalRisk[y][x]
            for (const { x: nx, y: ny } of proximity({ x, y })) {
                if (newRisk < totalRisk[ny][nx]) {
                    totalRisk[ny][nx] = newRisk
                    queue.push({ x: nx, y: ny })
                }
            }
        }
    }

const start = { x: 0, y: 0 }
totalRisk[start.y][start.x] = 0
lowest([{ x: 0, y: 0 }])
console.log(totalRisk[maxY * WID - 1][maxX * WID - 1])
