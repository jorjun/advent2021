// @jorjun

type Cell = { x: number; y: number }

const DATA = './day15/data.dat',
    grid = Deno.readTextFileSync(DATA)
        .split('\n')
        .map((row) => row.split('').map(Number)),
    totalRisk: number[][] = Array(grid.length)
        .fill(null)
        .map((_) => Array(grid[0].length).fill(Infinity)),
    [maxY, maxX] = [grid.length - 1, grid[0].length - 1],
    proximity = ({ x, y }: Cell) =>
        [
            [0, 1],
            [0, -1],
            [1, 0],
            [-1, 0],
        ]
            .map(([dx, dy]) => ({ x: x + dx, y: y + dy }))
            .filter(({ x, y }) => Math.min(x, y) >= 0 && x <= maxX && y <= maxY), // Grid bounds
    lowest = (queue: Cell[] = []) => {
        while (queue.length) {
            const { x, y } = queue.shift()!
            for (const { x: nx, y: ny } of proximity({ x, y })) {
                const newRisk = grid[y][x] + totalRisk[y][x]
                if (newRisk < totalRisk[ny][nx]) {
                    totalRisk[ny][nx] = newRisk
                    queue.push({ x: nx, y: ny })
                }
            }
        }
    }

const start = { x: 0, y: 0 }
totalRisk[start.y][start.x] = 0
lowest([{ x: 0, y: 0 }])
console.log(totalRisk[maxY][maxY])
