/** Advent of Code 2021, Day: 3
 
 @jorjun  V:vii ☉ ♐  ☽ ♐
 
 **/

import { readFileSync } from 'fs'

type ReadOut = [Array<number>, Array<number>]

const binaryToDec = (bits: number[]) => bits.reduce((prv, cur, ix) => prv + cur * 2 ** ix, 0)

class Sensor {
    data: string[]

    constructor(private path: string, private wordLen = 12) {
        const buff = readFileSync(`./${path}`)
        this.data = buff.toString().split('\n')
    }

    // yield most popular data bits for each readout column, from rightmost to leftmost
    *sample() {
        const newArray = () => new Array<number>(this.wordLen).fill(0),
            result: ReadOut = [newArray(), newArray()]
        for (let row of this.data) {
            for (let ix = 0; ix < this.wordLen; ix++) {
                const idx = row[ix] === '1' ? 1 : 0
                result[idx][ix] += 1
            }
        }
        for (let ix = this.wordLen - 1; ix >= 0; ix--) yield result[0][ix] > result[1][ix] ? 0 : 1
    }
    // Epsilon is the word complement of Gamma
    readout() {
        const bits: number[] = Array.from(this.sample()),
            epsilon = binaryToDec(bits),
            gamma = 2 ** this.wordLen - epsilon - 1
        return { epsilon, gamma }
    }
}

// Start
const sensor = new Sensor('day3/data.dat', 12),
    { gamma, epsilon } = sensor.readout()

console.log(gamma * epsilon)
