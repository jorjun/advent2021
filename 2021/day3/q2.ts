/** Advent of Code 2021, Day: 3, part 2
 
 @jorjun  V:vii ☉ ♐  ☽ ♐
 
 **/

import { readFileSync } from 'fs'

enum Gas {
    oxygen = '1',
    carbon = '0',
}

const binaryToDec = (bits: string) =>
    bits.split('').reduce((prv, cur, ix) => (cur === '0' ? prv : prv + 2 ** (bits.length - ix - 1)), 0)

class Life {
    data: string[]

    constructor(private path: string, private wordLen = 12) {
        const buff = readFileSync(`./${path}`)
        this.data = buff.toString().split('\n')
    }

    reading(bits: string[], gas: Gas, ix: number = 0): number {
        if (bits.length === 1) return binaryToDec(bits[0]) // distilled reading

        const isMajority = bits.filter((bit) => bit[ix] === gas).length > bits.length / 2,
            filtered = bits.filter((bit) => bit[ix] === (isMajority ? '1' : '0'))
        return this.reading(filtered, gas, ix + 1)
    }
}

// S T A R T  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ->
const sensor = new Life('./data.dat'),
    { o2, co2 } = {
        o2: sensor.reading([...sensor.data], Gas.oxygen),
        co2: sensor.reading([...sensor.data], Gas.carbon),
    }

console.log(o2 * co2)
