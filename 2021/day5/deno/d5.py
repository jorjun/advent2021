from math import copysign


raw = """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
"""

l1 = [rec for rec in raw.split('\n') if rec]
l2= [bunch.split('->') for bunch in l1]
l3 = [(a.split(","), b.split(",")) for a,b in l2]
l4 = [(int(x1), int(x2), int(x2), int(y2)) for (x1, x2), (y1, y2) in l3]

def norm(dd):
	return 0 if dd==0 else int(copysign(1,dd))
	
def param(a,b):
	d = norm(b-a)
	return {
		'a':a, 'b':b, 'd':d
	}

def lineIter(x1, y1, x2,y2):
	dx = x2-x1
	dy = y2-y1
	if dx!=0 and dy!=0: return False
	p1 = param(x1, x2)
	p2= param(y1,y2)
	if p1['d']!=0:
		a1=x1; a2=x2;d=p1['d'];b=x1
	else:
		a1=y1;a2=y2;d=p2['d'];b=y1
	
	for x in range(a1, a2+d, d):
			yield (b,x) if a1==x1 else (x,b)
			
for row in l4:
	print('.')
	for point in lineIter(*row):
		print (point) 
