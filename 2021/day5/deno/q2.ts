// @jorjun V:vii ☉ ♐  ☽ ♑

type Coord = { x: number; y: number }
type GridHash = {
    [hash: string]: number
}

function* range(a: number, b: number, inc: number) {
    if (inc === 0) return
    for (let i = a; i !== b + inc; i += inc) yield i
}

class Line {
    attr = {
        incX: 1,
        incY: 1,
    }
    constructor(private a: Coord, private b: Coord) {
        const [dx, dy] = [b.x - a.x, b.y - a.y],
            [aDx, aDy] = [dx, dy].map(Math.sign)
        this.attr = { incX: aDx, incY: aDy }
    }

    *points() {
        const { a, b, attr } = this,
            { incX, incY } = attr,
            gapX = Math.abs(a.x - b.x) + 1,
            gapY = Math.abs(a.y - b.y) + 1,
            xList = incX === 0 ? new Array<number>(gapY).fill(a.x) : Array.from(range(a.x, b.x, incX)),
            yList = incY === 0 ? new Array<number>(gapX).fill(a.y) : Array.from(range(a.y, b.y, incY))
        for (let ix = 0; ix < xList.length; ix++) yield { x: xList[ix], y: yList[ix] }
    }
}

class Thermal {
    grid: GridHash = {}
    lines: Line[] = []

    constructor(path: string) {
        for (const { a, b } of this.loadData(path)) {
            const [dx, dy] = [b.x - a.x, b.y - a.y]
            if (dx !== 0 && dy !== 0 && Math.abs(dx) !== Math.abs(dy)) {
                console.table({ dx, dy })
                continue
            }
            this.lines.push(new Line(a, b))
        }
        const { grid } = this
        for (const line of this.lines) {
            for (const { x, y } of line.points()) {
                const hash = `${x}:${y}`
                if (hash in grid) grid[hash] += 1
                else grid[hash] = 1
            }
        }
    }
    get score() {
        const overlap = Object.values(this.grid).filter((count) => count > 1)
        return overlap.length
    }

    *loadData(path: string) {
        const _buff = Deno.readTextFileSync(path),
            rawRows = _buff.split('\n')
        for (const _ of rawRows) {
            const [a, b] = _.split(' -> ')
                .map((lr: string) => lr.split(',').map(Number))
                .map(([x, y]) => ({
                    x,
                    y,
                }))
            yield { a, b }
        }
    }
}
const start = Date.now()
const thermal = new Thermal('./data.dat')
console.log(thermal.score)
const stop = Date.now()
console.log(`Time: ${stop - start} ms`)
