// @jorjun V:vii ☉ ♐  ☽ ♑

type Coord = { x: number; y: number }
type GridHash = {
    [hash: string]: number
}

function* range(a: number, b: number, inc: number) {
    for (let i = a; i !== b + inc; i += inc) yield i
}

class Line {
    attr = {
        inc: 1,
        vertical: false,
    }
    constructor(private a: Coord, private b: Coord) {
        const [dx, dy] = [b.x - a.x, b.y - a.y],
            [aDx, aDy] = [dx, dy].map(Math.sign)
        if (dx === 0) this.attr = { inc: aDy, vertical: true }
        else this.attr = { inc: aDx, vertical: false }
    }

    *points() {
        const { a, b, attr } = this,
            { vertical, inc } = attr,
            [start, end] = vertical ? [a.y, b.y] : [a.x, b.x]
        for (let i of range(start, end, inc)) yield vertical ? { x: this.a.x, y: i } : { x: i, y: this.a.y }
    }
}

class Thermal {
    grid: GridHash = {}
    lines: Line[] = []

    constructor(path: string) {
        for (const { a, b } of this.loadData(path)) {
            const [dx, dy] = [b.x - a.x, b.y - a.y]
            if (dx !== 0 && dy !== 0) continue
            this.lines.push(new Line(a, b))
        }
        const { grid } = this
        for (const line of this.lines) {
            for (const { x, y } of line.points()) {
                const hash = `${x}:${y}`
                if (hash in grid) grid[hash] += 1
                else grid[hash] = 1
            }
        }
    }
    get score() {
        const overlap = Object.values(this.grid).filter((count) => count > 1)
        return overlap.length
    }

    *loadData(path: string) {
        const _buff = Deno.readTextFileSync(path),
            rawRows = _buff.split('\n')
        for (let rawRow of rawRows) {
            const [a, b] = rawRow
                .split(' -> ')
                .map((lr: string) => lr.split(',').map(Number))
                .map(([x, y]) => ({
                    x,
                    y,
                }))
            yield { a, b }
        }
    }
}
const thermal = new Thermal('./day5/data.dat')
console.log(thermal.score)

// const test = [
//     [203, 123],
//     [1, 3],
//     [0, 1],
//     [1, 0],
// ]
// for (let [a, b] of test) {
//     const inc = Math.sign(b - a)
//     const r = Array.from(range(a, b, inc))
//     console.table(r)
// }
