// @jorjun

type Cell = [number, number]

const PATH = './day11/data.dat',
    _raw = Deno.readTextFileSync(PATH).split('\n'),
    rows = _raw.map((row) => `0${row}0`).map((row) => row.split('').map(Number)),
    rowLen = rows[0].length,
    grid = Array.from(getGrid()),
    neighbours = [
        [0, -1],
        [0, 1],
        [1, -1],
        [1, 0],
        [1, 1],
        [-1, -1],
        [-1, 0],
        [-1, 1],
    ]

function* getGrid() {
    const padRow = new Array<number>(rowLen).fill(0)
    yield padRow
    for (const row of rows) yield row
    yield padRow
}

function* iter(): Generator<Cell> {
    for (let i = 1; i < grid.length - 1; i++) for (let j = 1; j < rowLen - 1; j++) yield [i, j]
}

function output() {
    for (let i = 1; i < grid.length - 1; i++) {
        let row = ''
        for (let j = 1; j < rowLen - 1; j++) row += `${grid[i][j]}`
        console.log(row)
    }
    console.log('-----')
}

function step() {
    function* newGrid() {
        yield new Array<number>(rowLen).fill(0)
        for (const _ of rows) yield new Array<number>(rowLen).fill(0)
        yield new Array<number>(rowLen).fill(0)
    }
    function flashNeighbours([x, y]: Cell) {
        if (x < 1 || y < 1) return
        if (x > rowLen - 2 || y > rowLen - 2) return
        if (flashed[x][y] !== 0) return
        flashCount++
        flashTotal++
        flashed[x][y] = 1
        grid[x][y] = 0
        for (const [dx, dy] of neighbours) {
            try {
                const flash = flashed[x + dx][y + dy]
                if (flash !== 0) continue
            } catch (_err) {
                continue
            }
            grid[x + dx][y + dy] += 1
            if (grid[x + dx][y + dy] > 9) flashNeighbours([x + dx, y + dy])
        }
    }

    const flashed = Array.from(newGrid())
    let flashCount = 0,
        flashTotal = 0
    for (const [i, j] of iter()) grid[i][j] += 1 // Energy increase

    while (true) {
        flashCount = 0
        for (const [i, j] of iter()) {
            const cell = grid[i][j],
                flash = flashed[i][j]
            if (cell > 9 && flash === 0) flashNeighbours([i, j])
        }
        if (flashCount === 0) break // No flashing happened
    }
    return flashTotal
}

let total = 0
output()
for (let ix = 0; ix < 100; ix++) total += step()
output()
console.log(total)
