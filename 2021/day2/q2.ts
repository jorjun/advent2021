/** Advent of Code 2021, Day: 2
 
 @jorjun  V:vii ☉ ♐  ☽ ♏
 
 **/

import { readFile } from 'fs'
import { promisify } from 'util'

type Verb = keyof typeof Submarine.Cmd
type Command = {
    verb: Verb
    val: number
}

class Submarine {
    static Cmd = {
        forward: { dx: 1, dy: 0 },
        up: { dx: 0, dy: -1 },
        down: { dx: 0, dy: +1 },
    }
    pos = { x: 0, y: 0, dy: 0 }

    constructor(private commandList: Command[]) {}

    static async load(path: string) {
        const buff = await promisify(readFile)(`./${path}`),
            rows = buff.toString().split('\n')
        return rows.map((line): Command => {
            const row = line.split(' '),
                verb = row[0] as Verb,
                val = Number(row[1])
            return { verb, val }
        })
    }

    move(cmd: Command) {
        const { dx, dy } = Submarine.Cmd[cmd.verb],
            { pos } = this
        if (dx != 0) {
            pos.x += dx * cmd.val
            pos.y += pos.dy * cmd.val
        }
        if (dy != 0) pos.dy += dy * cmd.val
    }

    play() {
        for (let cmd of this.commandList) this.move(cmd)
        const { x, y } = this.pos
        return { x, y }
    }
}

if (require.main === module) {
    ;(async () => {
        const cmdList = await Submarine.load('data.dat'),
            sub = new Submarine(cmdList),
            { x, y } = sub.play()

        console.log(x * y)
    })()
}
