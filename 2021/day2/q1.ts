import { readFile } from 'fs'
import { promisify } from 'util'

type Verb = keyof typeof Submarine.Cmd

type Command = {
    verb: Verb
    val: number
}

class Submarine {
    static Cmd = {
        forward: { dx: 1, dy: 0 },
        up: { dx: 0, dy: -1 },
        down: { dx: 0, dy: +1 },
    }
    pos = { x: 0, y: 0 }

    move(cmd: Command) {
        const { dx, dy } = Submarine.Cmd[cmd.verb]
        this.pos.x += dx * cmd.val
        this.pos.y += dy * cmd.val
    }
}

async function load(path: String) {
    const rdr = await promisify(readFile)(`./${path}`),
        rows = rdr.toString().split('\n'),
        commandList = rows.map((line): Command => {
            const row = line.split(' '),
                verb = row[0] as Verb,
                val = Number(row[1])
            return { verb, val }
        })

    const sub = new Submarine()

    for (let cmd of commandList) {
        sub.move(cmd)
    }
    return sub.pos.x * sub.pos.y
}

if (require.main === module) {
    ;(async () => {
        const a3 = await load('data.dat')
        console.log(a3)
    })()
}
