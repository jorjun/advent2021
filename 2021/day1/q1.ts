import { readFile } from 'fs'
import { promisify } from 'util'

async function load(path: String) {
    const rdr = await promisify(readFile)(`./${path}`)
    const rows = rdr.toString().split('\n').map(Number)
    let count = 0
    for (let ix = 0; ix < rows.length - 1; ix++) {
        const first = rows[ix],
            second = rows[ix + 1]
        if (second > first) count++
    }
    return count
}

if (require.main === module) {
    ;(async () => {
        const a1 = await load('data.dat')
        console.log(a1)
    })()
}
