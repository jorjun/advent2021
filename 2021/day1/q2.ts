import { readFile } from 'fs'
import { promisify } from 'util'

async function load(path: String) {
    const rdr = await promisify(readFile)(`./${path}`)
    const rows = rdr.toString().split('\n').map(Number)
    let count = 0

    const sum = (data: Array<number>) => data.reduce((prv: number, cur: number) => cur + prv, 0)

    for (let ix = 0; ix < rows.length - 3; ix++) {
        const wdw1 = rows.slice(ix, ix + 3),
            wdw2 = rows.slice(ix + 1, ix + 4)
        const sum1 = sum(wdw1),
            sum2 = sum(wdw2)
        if (sum2 > sum1) count++
    }
    return count
}

if (require.main === module) {
    ;(async () => {
        const a1 = await load('data.dat')
        console.log(a1)
    })()
}
