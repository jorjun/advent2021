// @jorjun V:vii ☉ ♐  ☽ ♒

interface Row {
    key: string[]
    msg: string[]
}
type Lengths = [number, number]
interface Diff {
    p: Lengths
    k: string
}

type Dict = { [key: string]: string }

export const _raw = Deno.readTextFileSync('./day8/preview_data.dat').split('\n'),
    clean = (line: string) => line.split(' ').filter((_) => _ !== ''),
    data = _raw.map((row) => row.split('|')).map((pair) => ({ key: clean(pair[0]), msg: clean(pair[1]) })),
    diff = (a: string, b: string) => {
        if (a.length > b.length) [a, b] = [b, a]
        return a
            .split('')
            .filter((_) => b.indexOf(_) < 0)
            .join('')
    }

export class Code {
    real = ['abcefg', 'cf', 'acdeg', 'acdfg', 'bcdf', 'abdfg', 'abdefg', 'acf', 'abcdefg', 'abcdfg']
    one = ['c', 'f']
    four = ['b', 'c', 'd', 'f']
    seven = ['a', 'c', 'f']
    diffLen1: Diff[] = [
        // { p: [4, 7], k: 'a' },
        // { p: [4, 3], k: 'b' },
        // { p: [4, 5], k: 'c' },
        { p: [4, 0], k: 'd' },
    ]

    getLength(len: number) {
        return this.row.key.filter((_) => _.length === len)[0]
    }

    constructor(public row: Row) {}

    certs() {
        for (const { k, p } of this.diffLen1) {
            const [a, b] = p.map((_) => this.getLength(_))
            const d = diff(a, b)
            console.log(p, k, d)
        }
    }
}
const code = new Code(data[0])
code.certs()
