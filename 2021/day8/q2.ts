// @jorjun  V:vii ☉ ♐  ☽ ♓

type KeyDict = { [key: string]: string }
type DigitPad = { [key: string]: number }

const digits = ['abcefg', 'cf', 'acdeg', 'acdfg', 'bcdf', 'abdfg', 'abdefg', 'acf', 'abcdefg', 'abcdfg'],
    _raw = Deno.readTextFileSync('./day8/data.dat').split('\n'),
    clean = (line: string) => line.split(' ').filter((_) => _ !== ''),
    data = _raw.map((row) => row.split('|')).map((pair) => ({ key: clean(pair[0]), msg: clean(pair[1]) })),
    digitPad: DigitPad = {},
    sortWord = (word: string) => word.split('').sort().join(''),
    character = (who: string, values: string[]) =>
        values
            .filter((_) => _ !== who)
            .map((_) => diff(_, who).length)
            .sort()
            .join(''),
    diff = (x: string, y: string) => {
        const z = y // save !!
        for (const a of x) y = y.replace(a, '')
        for (const a of z) x = x.replace(a, '')
        return `${x}${y}`.split('').sort().join('')
    },
    getKey = (codes: string[]) => {
        const dict: KeyDict = {}
        for (let ix = 0; ix < 10; ix++) {
            const adept = digits[ix]
            let golden = ''
            for (let iy = 0; iy < 10; iy++) {
                const candidate = codes[iy]
                if (candidate.length !== adept.length) continue
                if (golden === '') golden = character(adept, digits)
                if (golden === character(candidate, codes)) {
                    dict[sortWord(candidate)] = sortWord(adept)
                    continue
                }
            }
        }
        return dict
    }

// ---------------------------------------------->
digits.forEach((code, ix) => (digitPad[code] = ix))
let total = 0
for (const { key, msg } of data) {
    const digitKey = getKey(key),
        display = msg
            .map((m) => sortWord(m))
            .map((m) => digitPad[digitKey[m]])
            .join('')
    total += Number(display)
}
console.log(total)
