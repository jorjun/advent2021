// Day 14 Advent of Code 2021  @jorjun V: vii ☉ ♐  ☽ ♉

/** I had to give up on this one. I missed out on that precious Eureka! moment by cheating.
 * Nevertheless once I got the correct idea, I implemented without reference to the code I had seen.
 * Mine will be more elegant at least.
 */

type PolyCount = { [lett: string]: number }
type Instruction = { [code: string]: string }

const DATA = './day14/preview_data.dat',
    _raw = Deno.readTextFileSync(DATA).split('\n'),
    template = _raw[0],
    instruction: Instruction = {},
    loadInstructions = () => {
        for (const row of _raw.slice(1)) {
            const _blankRow = row === ''
            if (_blankRow) continue
            const [key, val] = row.split(' -> ')
            instruction[key] = val
        }
    },
    newPair = (pair: string) => {
        // NN => {ch:C} => [NC, CN] --NN
        const ch = instruction[pair]
        return [`${pair[0]}${ch}`, `${ch}${pair[1]}`]
    },
    polyCount: PolyCount = {}, // Counting every poly letter
    pairGen: PolyCount = {} // Tracking pairs

function* pairs(word: string) {
    for (let ix = 0; ix < word.length - 1; ix++) yield `${word[ix]}${word[ix + 1]}`
}

loadInstructions()

Array.from(pairs(template)).map((_) => (pairGen[_] = 1))
console.log(polyCount)

for (let step = 1; step <= 2; step++) {
    for (const pair of Object.keys(pairGen)) {
        for (const newP of newPair(pair)) {
            console.log(pair, newP)
            for (const ch of newP)
                if (ch in polyCount) polyCount[ch] += polyCount[ch]
                else polyCount[ch] = 1
        }
    }

    console.log(polyCount)
    // Every key in pairGen
}
