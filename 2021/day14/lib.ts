// Day 13 Advent of Code 2021  @jorjun V: vii ☉ ♐  ☽ ♉

/** I am too far behind (today is day 16) to abstract the solution into classes to reduce noisy namespace clutter and layer everything semantically.
  Can't resist combing and tidying tho' :D  
  Whether you can take pride in your work or you are merely the tactical slave utensil (TSU) of a politico-pension borg (PPB) is your own choice to make.
*/

export type Fold = { axis: string; val: number }
type Group = { [lett: string]: number }
interface Instruction {
    [code: string]: string
}

export const DATA = './day14/data.dat',
    _raw = Deno.readTextFileSync(DATA).split('\n'),
    template = _raw[0],
    instruction: Instruction = {}

export function loadData() {
    for (const row of _raw.slice(1)) {
        const _blankRow = row === ''
        if (_blankRow) continue
        // parse rows into key=>insertion letter
        const [key, val] = row.split(' -> ')
        instruction[key] = val
    }
}

export function* pairs(word: string) {
    let last = ''
    for (let ix = 0; ix < word.length - 1; ix++) {
        const [ch1, ch2] = [word[ix], word[ix + 1]],
            key = `${ch1}${ch2}`,
            insert = instruction[key]
        last = ch2
        yield `${ch1}${insert}`
    }
    yield last
    // yield `${ch1}${instruction[key]}${ch2}`
}

loadData()
console.log(instruction)

let word = template
for (let step = 1; step <= 40; step++) {
    word = Array.from(pairs(word)).join('')
    const group = word.split('').reduce((group: Group, cur: string) => {
        group[cur] = (group[cur] ?? 0) + 1
        return group
    }, {})
    const freq = Object.entries(group)
        .map((rec) => {
            return { l: rec[0], v: rec[1] }
        })
        .sort((a, b) => a.v - b.v)
    const result = { step, word, freq, big: freq[freq.length - 1], small: freq[0] }
    console.log(result.big.v - result.small.v)
}
