// @jorjun

const Score = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137,
    },
    Pairs = {
        '(': ')',
        '[': ']',
        '{': '}',
        '<': '>',
    },
    Closed = [')', ']', '}', '>']

type Stack = { ix: number; expect: string }
type Result = { ix?: number; chunk?: string; bracket: string; close?: string; error?: string }
type Bracket = keyof typeof Pairs

class Tokeniser {
    rows: string[] = []

    constructor(path: string) {
        this.rows = Deno.readTextFileSync(path).split('\n')
    }

    parse(text: string) {
        const stack: Stack[] = []
        let ix = 0
        for (const bracket of text) {
            if (Closed.includes(bracket)) {
                const { ix, expect } = stack.pop() ?? { ix: 0, expect: null }
                if (expect !== bracket) {
                    const score = Score[bracket as keyof typeof Score]
                    return { err: 'corrupt', ix, bracket, expect, score }
                }
            } else stack.push({ ix, expect: Pairs[bracket as Bracket] })
            ix++
        }
        return { ix }
    }
}

// ----------------------------------------------------------------------->
const token = new Tokeniser('./day10/preview_data.dat')
let total = 0
for (const row of token.rows) {
    const result = token.parse(row)
    if (result.err) {
        const { ix, score } = result,
            from = row.slice(0, ix - 1),
            invalid = row.slice(ix, ix + 1),
            rest = row.slice(ix + 1)
        total += score
        console.log(`Score: ${score} ${from} *${invalid}* ${rest}`)
    }
}
console.log({ total })
