// @jorjun

const Pairs = {
        '(': ')',
        '[': ']',
        '{': '}',
        '<': '>',
    },
    Closed = [')', ']', '}', '>']

type Stack = { expect: string }
type Bracket = keyof typeof Pairs

class Tokeniser {
    rows: string[] = []

    constructor(path: string) {
        this.rows = Deno.readTextFileSync(path).split('\n')
    }

    complete(text: string) {
        const stack: Stack[] = [],
            Scoring = {
                ')': 1,
                ']': 2,
                '}': 3,
                '>': 4,
            }
        for (const bracket of text) {
            if (Closed.includes(bracket)) {
                const { expect } = stack.pop() ?? { expect: null }
                if (expect !== bracket) return { error: true, totalScore: 0 }
            } else stack.push({ expect: Pairs[bracket as Bracket] })
        }
        const cText = stack.reverse().map((_) => _.expect)
        const scores = cText.map((_) => Scoring[_ as keyof typeof Scoring])
        const totalScore = scores.reduce((prv, cur) => prv * 5 + cur, 0)

        return { error: false, cText, totalScore, scores }
    }
}

// ----------------------------------------------------------------------->
const token = new Tokeniser('./day10/data.dat')
let scores = []
for (const row of token.rows) {
    const rec = token.complete(row)
    if (rec?.error) continue
    console.log(rec)
    scores.push(rec.totalScore)
}
scores.sort((a, b) => a - b)
const mid = Math.floor(scores.length / 2)
console.log(scores, scores[mid])
