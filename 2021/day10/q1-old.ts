const Pair = {
    '(': {
        score: 3,
        close: ')',
    },
    '[': {
        score: 57,
        close: ']',
    },
    '{': {
        score: 1197,
        close: '}',
    },
    '<': {
        score: 25137,
        close: '>',
    },
}
type Bracket = keyof typeof Pair
type Result = { ix?: number; chunk?: string; bracket: Bracket; close?: string; error?: string }

const rows = Deno.readTextFileSync('./day10/preview_data.dat').split('\n')

console.table(rows)

class Token {
    parsed: Result[] = []
    constructor(public rows: string[]) {}
    *eat(row: string, bracket: Bracket): Generator<Result> {
        const close = Pair[bracket].close,
            chunkEndX = row.lastIndexOf(close) + 1
        if (!close) {
            yield { bracket, chunk: row, error: `Bad: open ${bracket}` }
            return
        }

        if (chunkEndX === 0) {
            yield {
                bracket,
                close,
                chunk: row,
                error: `No close bracket`,
            }
            return
        }
        const chunk = {
            bracket,
            chunk: row.slice(0, chunkEndX),
            close,
            ix: chunkEndX,
        }
        yield chunk
    }
    parse(text: string): Result | undefined {
        if (text === '') return
        const bracket = text[0] as Bracket
        for (const chunk of this.eat(text, bracket)) {
            if (!chunk.error) this.parsed.push(chunk)
            else {
                console.log(chunk)
                continue
            }
            if (!chunk.chunk) continue
            const rest = text.slice(chunk.ix)
            return this.parse(rest)
        }
    }
}

const token = new Token(rows)
for (const row of token.rows) {
    const result = token.parse(row)
    if (result) {
        console.log(result)
    }
    console.log('Chunks:')
    for (const chunk of token.parsed) {
        console.log(chunk)
    }
    break
}
