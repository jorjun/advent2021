// @jorjun  V:vii ☉ ♐  ☽ ♑

const loadData = (path: string) => Deno.readTextFileSync(path).split(',').map(Number)

function countFish(age: number, days: number, count = 0) {
    if (days === 0) return 1

    const key = `${age}${days}`
    if (key in memo) return memo[key]

    if (age === 0) {
        count += countFish(6, days - 1)
        count += countFish(8, days - 1)
    } else count += countFish(age - 1, days - 1)

    memo[key] = count
    return count
}

// Start ------------------------------------------------------------------>
const DAYS = 256
const memo: { [key: string]: number } = {}

let sum = 0
for (const fish of loadData('./day6/data.dat')) sum += countFish(fish, DAYS)
console.log(sum)
