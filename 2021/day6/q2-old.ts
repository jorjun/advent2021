import { readFileSync } from 'fs'

function loadData(path: string) {
    // const _buff = Deno.readTextFileSync(path)
    const _buff = readFileSync(path).toString()
    return _buff.split(',').map(Number)
}

const ans = new Uint8ClampedArray(26984457539)

const DAYS = 256,
    fish = loadData('./day6/preview_data.dat')
fish.forEach((num: number, ix: number) => (ans[ix] = num))

let count = fish.length
for (let day = 0; day < DAYS; day++) {
    const stock = count
    for (let ix = 0; ix < stock; ix++) {
        if (ans[ix] === 0) {
            ans[ix] = 6
            ans[count++] = 8
        } else ans[ix] -= 1
        // console.log(day + 1, ans.slice(0, count))
    }
}
console.log(count)
