function loadData(path: string) {
    const _buff = Deno.readTextFileSync(path)
    return _buff.split(',').map(Number)
}

const getAge = (age: number) => {
    let spawn
    if (age === 0) {
        age = 6
        spawn = true
    } else {
        age -= 1
        spawn = false
    }
    return { age, spawn }
}

const DAYS = 80,
    fish = loadData('./day6/data.dat')
console.log(fish)
for (let day = 0; day < DAYS; day++) {
    const school = fish.length
    for (let ix = 0; ix < school; ix++) {
        const gen = getAge(fish[ix])
        fish[ix] = gen.age
        if (gen.spawn) fish.push(8)
    }
    console.log(day, fish)
}
const sum = fish.length
console.log(sum)
