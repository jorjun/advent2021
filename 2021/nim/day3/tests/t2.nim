import q2, unittest

suite "AOC day 03":
    setup:
        let data: Data = @[
            "00100",
            "11110",
            "10110",
            "10111",
            "10101",
            "01111",
            "00111",
            "11100",
            "10000",
            "11001",
            "00010",
            "01010"
        ]

    test "Part 2":
        let life = newLife(data)
        check life.lifeSupportRating == 230
