# @jorjun

import
  strutils,
  sequtils

type Data* = seq[string]

type
  Life = object
    data: Data

  Gas = enum
    co2 = '0'
    o2 = '1'

proc moreOnes(data: Data, idx: int): Gas =
  let
    half = data.len div 2
    even = data.len mod 2 == 0
    ones = data.countIt(it[idx] == '1')
  if (ones > half) or (even and ones == half): Gas.o2 else: Gas.co2

proc siftData(data: Data, gas: Gas, idx: int = 0): int =
  if data.len == 1:
    return data[0].parseBinInt
  let
    bit = if moreOnes(data, idx) == gas: '1' else: '0'
    subdata = data.filterIt(it[idx] == bit)
  siftData(subData, gas, idx + 1)


method reading*(this: Life): int =
  let
    o2 = siftData(this.data, Gas.o2)
    co2 = siftData(this.data, Gas.co2)
  o2 * co2


when isMainModule:
  let
    data = readFile("data/day3/data.dat").splitLines
    life = Life(data: data)
  echo life.reading
