# Package

version       = "0.1.0"
author        = "@jorjun"
description   = "Advent of Code 2021"
license       = "MIT"
srcDir        = "src"
bin           = @["nim"]


# Dependencies

requires "nim >= 1.6.0"

task test, "Runs the test suite":
  exec "nim c --outdir:bin/ -r src/day3/tests/t2.nim"
