export function* collector() {
    let row: string = yield null // Receive first row
    const buff = new Array<string[]>(2)
    while (row) {
        if (row.indexOf('|') > 1) {
            buff[0] = row
                .split('|')[0]
                .split(' ')
                .filter((_) => _ != '')
            row = yield null // fetch next
        } else {
            buff[1] = row.split(' ')
            const [left, right] = buff
            row = yield { left, right }
        }
    }
}


export function gcd(a: number, b: number): number {
    while (b > 0) {
        let tmp = a
        a = b
        b = tmp % b
    }
    return a
}

export function lcm(array: number[]): number {
    if (array.length === 0) throw new Error('Array must not be an empty array')
    const prod = array[0]
    for (const el of array) prod *= el / gcd(prod, el)
    return prod
}

export function* parse() {
    const collect = collector()
    collect.next() // Collect start
    for (const raw of data) {
        const p = collect.next(raw).value
        if (p) yield p
    }
    collect.next() // Collect end
}
