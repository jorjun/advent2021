defmodule Data do
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n")
  end
  
end
