defmodule Elf do

  Code.require_file("day_03/lib/data.ex")
 
         
  def q1(path) do
    path
      |> Data.load()
      |> Enum.map(fn(v)-> String.split_at(v, trunc(String.length(v)/2)) end)
      |> Enum.map(fn({l,r})-> {level(l),level(r)} end)
      |> in_both([])
      |> prioritize([])
  end
  
  def q2(path) do
    path
      |> Data.load()
      |> Enum.chunk_every(3)
      |> Enum.map(&Enum.map(&1, fn(v)-> level(v) end))
      |> in_all([])
      |> prioritize([])
  end
  
  defp level(v) do
    v
      |> String.graphemes()
      |> Enum.uniq()
  end
    
  defp in_both([], acc), do: acc
  defp in_both([{l,r}|tail], acc) do
    diff = l -- r
    r = l -- diff
    new_acc = List.insert_at(acc, -1, hd(r))
    in_both(tail, new_acc)
  end
  
  defp in_all([],acc), do: acc
  defp in_all([[a,b,c]|tail], acc) do
    
    d1 = a -- b
    d2 = b -- c
    
    b1 = a -- d1
    b2 = b -- d2
    
    f = b1 -- b2
    r = b1 -- f
    
    new_acc = List.insert_at(acc, -1, hd(r))
    in_all(tail, new_acc)
  end
  
  defp prioritize([],acc), do: acc
  defp prioritize([<<head::utf8>>|tail], acc) do
    priority =
      case head do
        x when x > 96 and x < 123 -> ((head - 97) + 1)
        x when x > 64 and x < 91 ->((head - 65) + 27)
      end
    new_acc = List.insert_at(acc, -1, priority)
    prioritize(tail, new_acc)
  end
end

IO.puts("TEST")

Elf.q1("day_03/data/test.txt") |> Enum.sum() |> IO.inspect(label: "score")
Elf.q2("day_03/data/test.txt") |> Enum.sum() |> IO.inspect(label: "new guide score")
#
IO.puts("\nACTUAL")
#
Elf.q1("day_03/data/d3_1.txt") |> Enum.sum() |> IO.inspect(label: "score")
Elf.q2("day_03/data/d3_1.txt") |> Enum.sum() |> IO.inspect(label: "new guide score")