defmodule Elf do

  Code.require_file("day_06/lib/data.ex")
  
  @marker_count 3
  @message_count 13
  
  def q1(path, len \\  @marker_count) do
    path
      |> Data.load()
      |> first_marker( len, %{count: len} )
  end
  
  def q2(path), do: q1(path, @message_count)
  
  def first_marker([], _, acc), do: Map.drop(acc, [:count])
  def first_marker([head|tail], len, acc) do
    {m_buf, rest} = Enum.split(head, len)
    new_acc = match_frame(rest, len, m_buf, acc)
    first_marker(tail, len, new_acc)
  end
  
  def match_frame([], len, m_buf, acc) do
     Map.put(acc, m_buf, %{count: Map.get(acc,:count, 0)})
     |> Map.put(:count, len)
  end
       
  def match_frame([head|tail], len, m_buf, acc) do
    new_buf =  List.insert_at(m_buf, -1, head) 
    {new_tail, new_buf, new_acc} = found?(tail, len, new_buf, acc)
    match_frame(new_tail, len, new_buf, new_acc)
  end
  
  #not found
  defp found?(x, len, tail, frame, acc) when x < (len + 1) do
    {_, new_buf} = List.pop_at(frame, 0)
    {tail, new_buf, increment(acc)}
  end
  
  # found
  defp found?(x, len, _, frame, acc) when x == (len + 1), do: {[], frame, increment(acc)}
  
  defp increment(acc) do
    acc
    |> Map.get_and_update(:count, fn(c)-> {c, c + 1} end)
    |> then(fn({_, new})-> new end)
  end
  
  defp found?(tail, len, frame, acc) do
    check = Enum.uniq(frame) |> Enum.count()
    found?(check, len, tail, frame, acc)
  end
  
end

IO.puts("TEST")
    Elf.q1("day_06/data/test.txt") |> IO.inspect(label: "top")
    Elf.q2("day_06/data/test.txt")|> IO.inspect(label: "new top")
#
IO.puts("\nACTUAL")
#  #    # #
   Elf.q1("day_06/data/d6_1.txt")|> IO.inspect(label: "top")
   Elf.q2("day_06/data/d6_1.txt")|> IO.inspect(label: "new top")
#