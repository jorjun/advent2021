defmodule Elf do

  Code.require_file("day_02/lib/data.ex")
 
  @rock 1
  @paper 2
  @scissors 3
  
  @lose 0
  @draw 3
  @win 6
         
  def q1(path) do
    path
      |> Data.load()
      |> play(0)
  end
  
  def q2(path) do
    path
      |> Data.load()
      |> play2(0)
  end

  defp play([], acc), do: acc
  
  defp play([[op, me]|tail], acc) do
    new_acc = acc + score(op, me)
    play(tail, new_acc)
  end
  
  defp play2([], acc), do: acc
  
  defp play2([[op, me]|tail], acc) do
    new_acc = acc + score2(op, me)
    play2(tail, new_acc)
  end
    
  defp score("A","X"),  do: @rock + @draw # draw rock v rock
  defp score("B", "Y"), do: @paper + @draw # draw paper v paper
  defp score("C", "Z"), do: @scissors + @draw # draw scissors v scissors
  
  defp score("C", "X"), do: @rock + @win # win scissors v rock
  defp score("A", "Y"), do: @paper + @win # win rock v paper
  defp score("B", "Z"), do: @scissors + @win # win paper v scissors
 
  defp score("B", "X"), do: @rock + @lose # lose paper v rock
  defp score("C", "Y"), do: @paper + @lose # lose scissors v paper
  defp score("A", "Z"), do: @scissors + @lose # lose rock v scissors
  
  defp score(a,b),  do: IO.puts "oops #{a}, #{b}"
  
  defp score2("B", "X"), do: @rock + @lose # lose paper choose rock
  defp score2("C", "X"), do: @paper + @lose # lose scissors choose paper
  defp score2("A","X"),  do: @scissors + @lose # lose rock choose scissors
  
  defp score2("A", "Y"), do: @rock + @draw # draw rock choose rock
  defp score2("B", "Y"), do: @paper + @draw # draw paper choose paper
  defp score2("C", "Y"), do: @scissors + @draw # draw scissors choose scissors
  
  defp score2("C", "Z"), do: @rock + @win # win scissors choose rock
  defp score2("A", "Z"), do: @paper + @win # win rock choose paper
  defp score2("B", "Z"), do: @scissors + @win # win paper choose scissors
  
  defp score2(a,b), do: IO.puts "oops #{a}, #{b}"
end

IO.puts("TEST")

Elf.q1("day_02/data/test.txt") |> IO.inspect(label: "score")
Elf.q2("day_02/data/test.txt") |> IO.inspect(label: "new guide score")

IO.puts("\nACTUAL")

Elf.q1("day_02/data/d2_1.txt") |> IO.inspect(label: "score")
Elf.q2("day_02/data/d2_1.txt") |> IO.inspect(label: "new guide score")