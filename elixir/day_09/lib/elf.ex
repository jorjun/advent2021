defmodule Elf do
  
  Code.require_file("day_09/lib/data.ex")
        
  def up({x, y}),                                 do: {x, y + 1}
  def right({x, y}),                              do: {x + 1, y}
  def down({x, y}),                               do: {x, y - 1} 
  def left({x, y}),                               do: {x - 1, y}
  
  def dne(p),                                     do: p |> up()   |> right() #Due North East
  def dnw(p),                                     do: p |> up()   |> left()  #Due North West
  def dse(p),                                     do: p |> down() |> right() #Due South East
  def dsw(p),                                     do: p |> down() |> left()  #Due South West
  
  def touching?({_,{x1, y1}, _}, {_,{x, y}, _}),  do: (x >= (x1 - 1) and x <= (x1 + 1) and y >= (y1 - 1) and y <= (y1 + 1))
  def row_col?({_,{x, y}, _}, {_,{x1, y1}, _}),   do: x == x1 or y == y1   
  def diagonal?({hx, hy}, {tx, ty}),              do: (abs(hx - tx) == abs(hy - ty))
  
  def update_at(knots, {i, _, _} = what),         do: List.replace_at(knots, i, what)
  def build_knots(count),                         do: for n <- 0..count,  do: {n, {4, 4}, MapSet.new([{4 ,4}])} 
  
  def step_head({"U", amount}, {i, p, m}, knots), do: {{"U", amount - 1}, update_at(knots, {i, up(p), m})}
  def step_head({"R", amount}, {i, p, m}, knots), do: {{"R", amount - 1}, update_at(knots, {i, right(p), m})}
  def step_head({"D", amount}, {i, p, m}, knots), do: {{"D", amount - 1}, update_at(knots, {i, down(p), m})}
  def step_head({"L", amount}, {i, p, m}, knots), do: {{"L", amount - 1}, update_at(knots, {i, left(p), m})}
  
  def step_tail("U", {i, p, m}, knots),           do: update_at(knots, {i, up(p),    MapSet.put(m, up(p))})
  def step_tail("R", {i, p, m}, knots),           do: update_at(knots, {i, right(p), MapSet.put(m, right(p))})
  def step_tail("D", {i, p, m}, knots),           do: update_at(knots, {i, down(p),  MapSet.put(m, down(p))})
  def step_tail("L", {i, p, m}, knots),           do: update_at(knots, {i, left(p),  MapSet.put(m, left(p))})
  
  def step_tail("dne", {i, p, m}, knots),         do: update_at(knots, {i, dne(p),  MapSet.put(m, dne(p))})
  def step_tail("dnw", {i, p, m}, knots),         do: update_at(knots, {i, dnw(p),  MapSet.put(m, dnw(p))})
  def step_tail("dse", {i, p, m}, knots),         do: update_at(knots, {i, dse(p),  MapSet.put(m, dse(p))})
  def step_tail("dsw", {i, p, m}, knots),         do: update_at(knots, {i, dsw(p),  MapSet.put(m, dsw(p))})
  
  def step([], knots),                            do: knots
  def step([{_, 0}|tail], knots),                 do: step(tail, knots)
  
  def step([{dir, _} = direction|rest], [head|_] = knots) do
    {new_dir, new_knots} = step_head(direction, head, knots)
    Data.rplot(dir, new_knots)
    knot_compare = Enum.chunk_every(new_knots, 2, 1, :discard)
    step(dir, [new_dir|rest], knot_compare, new_knots)
  end
  
  def step(_, cont, [], knots),  do: step(cont, knots)
  def step(dir, cont, [[{a, _, _}, {b, _, _}]|snake], knots) do
    head = Enum.at(knots, a)
    seg  = Enum.at(knots, (b))
    new_knots = if touching?(head, seg), do: knots, else: follow?(dir, head, seg, knots) |> then(&Data.rplot(dir, &1))
    step(dir, cont, snake, new_knots)
  end
      
  def follow?(dir, {_, head, _} = p1, {_, {x1, y1}, _} = p2, knots) do
    if row_col?(p1, p2) do
      case head do
        {x, y} when x == x1 and y == y1 -> step_tail(dir, p2, knots)
        {x, y} when x == x1 -> step_row(dir, y, p2, knots)
        {x, y} when y == y1 -> step_column(dir, x, p2, knots) 
      end
    else
      case head do
        {x, y} when x > x1 and y > y1 -> step_tail("dne", p2, knots)
        {x, y} when x > x1 and y < y1 -> step_tail("dse", p2, knots)
        {x, y} when x < x1 and y < y1 -> step_tail("dsw", p2, knots)
        {x, y} when x < x1 and y > y1 -> step_tail("dnw", p2, knots)
      end
    end
  end
  
  def step_column(_dir, x, {_, {x1, _}, _} = p2, knots) do
    case x do
      x when x > x1 -> step_tail("R", p2, knots)
      x when x < x1 -> step_tail("L", p2, knots)
    end
  end
  
  def step_row(_dir, y, {_, {_, y1}, _} = p2, knots) do
    case y do
      y when y > y1 -> step_tail("U", p2, knots)
      y when y < y1 -> step_tail("D", p2, knots)
    end
  end
  
  def q1(path, knots \\ 1) do # zero based == 2
    path
      |> Data.load()
      |> step(build_knots(knots))
  end
  
  def q2(path, knots \\ 9) do # zero based == 10
    path
      |> Data.load()
      |> step(build_knots(knots))
  end
  
end

IO.puts("TEST")
# Elf.q1("day_09/data/test.txt") |> IO.inspect(label: "visited")
# Elf.q2("day_09/data/test2.txt") |> Enum.map(fn({n, _, m})->{n, MapSet.size(m)} end) |> IO.inspect(label: "visited")
#
# IO.puts("ACTUAL")
#
# Elf.q1("day_09/data/d9_1.txt") |> List.last() |> then( fn({_, _, m}) -> MapSet.size(m)end) |> IO.inspect(label: "new visited")
Elf.q2("day_09/data/d9_1.txt") |> Enum.map(fn({n, _, m})->{n, MapSet.size(m)} end) |> IO.inspect(label: "new visited")