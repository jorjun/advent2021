defmodule Data do
  
  @frame 30
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(fn([d, v])-> {d, String.to_integer(v)} end)
  end
  
  def rplot(dir, data) do 
    data
      |> Enum.reverse() 
      |> then(&plot(dir, &1))
  end
  
  def plot(grid, [], data) do
    IO.ANSI.cursor(2, 1) |> IO.puts
    :timer.sleep(20)
    grid
      |> Enum.map(&Enum.join/1)
      |> Enum.map(&IO.inspect(&1, width: 200))
    Enum.reverse(data)
  end
  
  def plot(grid, [{_, {hx, hy},_}|[]], data) do
    {hfx, hfy} = {abs(rem(hx, @frame)) + 1, abs(rem(hy, @frame)) + 1}
    grid
      |> Enum.at(@frame - hfy)
      |> then(&List.replace_at(&1, hfx, "H"))
      |> then(&List.replace_at(grid, (@frame - hfy), &1))
      |> plot([], data)
  end
  
  def plot(grid, [{sa, {hx, hy},_}|tail], data) do
    {hfx, hfy} = {abs(rem(hx, @frame)) + 1, abs(rem(hy, @frame)) + 1}
    grid
      |> Enum.at(@frame - hfy)
      |> then(&List.replace_at(&1, hfx, "#{sa}"))
      |> then(&List.replace_at(grid, (@frame - hfy), &1))
      |> plot(tail, data)
  end
  
  def plot(dir, [{_, {hx, hy},_}|tail] = data) do
    line = for _ <- 0..@frame, do: "."
    grid = for _ <- 0..@frame, do: line
  
    IO.inspect(dir, label: "direction")
    
    {hfx, hfy} = {abs(rem(hx, @frame)) + 1, abs(rem(hy, @frame)) + 1}

    grid
      |> Enum.at(@frame - hfy)
      |> then(&List.replace_at(&1, hfx, "T"))
      |> then(&List.replace_at(grid, (@frame - hfy), &1))
      |> plot(tail, data)
  end
  
end
