defmodule Data do
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n")
      # |> Enum.map(&String.split(&1, "\n"))
  end
  
end

# top: %{
#   "/" => ["dir a", "14848514 b.txt", "8504156 c.dat", "dir d"],
#   "a" => ["dir e", "29116 f", "2557 g", "62596 h.lst"],
#   "d" => ["4060174 j", "8033020 d.log", "5626152 d.ext", "7214296 k"],
#   "e" => ["584 i"]
# }