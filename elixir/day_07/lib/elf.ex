defmodule Elf do
  Code.require_file("day_07/lib/data.ex")

  @avail 70_000_000
  @required 30000000
  
  def q1(path) do
    path
      |> Data.load()
      |> parse([], %{})
      |> do_sum()
  end

  def q2(path) do
    acc =
      path
      |> Data.load()
      |> parse([], %{})

    used = get_in(acc, ["/","total"])
    unused = @avail - used
    needed = @required - unused

    get_totals(acc, [])
      |> Enum.reject(fn(v)-> v < needed end)
      |> Enum.min()
  end

  def do_sum2(acc) do
    acc
      |> get_totals([])
      |> Enum.reject(fn v -> v >= 100_000 end)
      |> Enum.sum()
  end

  def do_sum(acc) do
    acc
      |> get_totals([])
      |> Enum.reject(fn v -> v >= 100_000 end)
      |> Enum.sum()
  end

  def get_totals(original, acc) when is_map(original) do
    original
      |> Map.keys()
      |> get_totals(original, acc)
  end

  def get_totals(_, acc), do: acc

  def get_totals([], _, acc), do: acc

  def get_totals([head | tail], original, acc) do
    new_acc =
      case head do
        "total" -> List.insert_at(acc, -1, Map.get(original, head))
        _ -> get_totals(Map.get(original, head), acc)
      end

    get_totals(tail, original, new_acc)
  end

  def parse([], _, acc), do: acc

  def parse(["$ cd .." | tail], [_ | new_path], acc), do: parse(tail, new_path, acc)

  def parse(["$ cd " <> dir | tail], path, acc) do
    new_path = List.insert_at(path, 0, dir)
    parse(tail, new_path, acc)
  end

  def parse(["$ ls" | tail], path, acc) do
    nxt_cmd =
      Enum.find_value(tail, fn(v) ->
        if String.starts_with?(v, "$") do
          v
        end
      end)

    grab =
      case nxt_cmd do
        nil -> Enum.count(tail)
        nxt_cmd -> Enum.find_index(tail, fn v -> v == nxt_cmd end)
      end

    {grabbed, new_tail} = Enum.split(tail, grab)

    row =
      grabbed
        |> Enum.map(&String.split(&1, " "))
        |> Enum.into(%{}, &convert/1)

    total =
      row
        |> Map.values()
        |> Enum.reject(fn v -> v == "dir" end)
        |> Enum.sum()

    tmp_acc = put_in(acc, Enum.reverse(path), Map.put(row, "total", total))
    [_ | parent] = path
    new_acc = update_parent(parent, total, tmp_acc)
    parse(new_tail, path, new_acc)
  end

  def update_parent([], _, acc), do: acc

  def update_parent([_ | new_parent] = parent, total, acc) do
    {_, new_acc} = get_and_update_in(acc, Enum.reverse(parent) ++ ["total"], fn c -> {c, c + total} end)
    update_parent(new_parent, total, new_acc)
  end

  def convert(["dir", v]), do: {v, "dir"}
  def convert([k, v]), do: {v, String.to_integer(k)}
end

IO.puts("TEST")
Elf.q1("day_07/data/test.txt") |> IO.inspect(label: "top")
Elf.q2("day_07/data/test.txt") |> IO.inspect(label: "new top")
# #
IO.puts("\nACTUAL")
# #
Elf.q1("day_07/data/d7_1.txt") |> IO.inspect(label: "top")
Elf.q2("day_07/data/d7_1.txt")|> IO.inspect(label: "new top", limit: :infinity)
# #
