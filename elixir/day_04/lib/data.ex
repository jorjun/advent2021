defmodule Data do
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&String.split(&1,","))
  end
  
end
