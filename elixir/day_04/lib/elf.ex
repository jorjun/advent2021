defmodule Elf do

  Code.require_file("day_04/lib/data.ex")
  
  def q1(path) do
    path
      |> Data.load()
      |> to_ranges([])
      |> subset?([])
  end
  
  def q2(path) do
    path
      |> Data.load()
      |> to_ranges([])
      |> overlap?([])
  end
  
  defp to_ranges([], acc), do: acc
  defp to_ranges([[l,r]|tail], acc) do
    acc
      |> List.insert_at(-1, [rangeify(l),rangeify(r)])
      |> then(&to_ranges(tail, &1))
  end
  
  defp overlap?([], acc), do: acc
  defp overlap?([[l, r]|tail], acc) do
    new_acc = 
    case Range.disjoint?(l, r) do
      true -> acc
      false -> List.insert_at(acc, -1,[l, r])
    end
    overlap?(tail, new_acc)
  end
    
  defp subset?([], acc), do: acc
  defp subset?([[l,r]|tail], acc) do
    a = Enum.to_list(l)
    b = Enum.to_list(r)
    new_acc = 
    case contains?(a, b) do
      false -> case contains?(b, a) do
                true -> List.insert_at(acc, -1,[b, a])
                false -> acc
               end
      true -> List.insert_at(acc, -1,[a, b])
    end
    subset?(tail, new_acc)
  end
  
  def contains?([], _), do: false
  def contains?(container, contained) do
    case List.starts_with?(container, contained) do
      true -> true
      false -> contains?(tl(container), contained)
    end
  end
    
  defp rangeify(d) do
    [a,b] =
    d
      |> String.split("-")
      |> Enum.map(&String.to_integer/1)
      
    a..b
  end
  
end
    IO.puts("TEST")
    Elf.q1("day_04/data/test.txt") |> Enum.count() |> IO.inspect(label: "total")
    Elf.q2("day_04/data/test.txt") |> Enum.count() |> IO.inspect(label: "new total")
    # #
    IO.puts("\nACTUAL")
    # #
    Elf.q1("day_04/data/d4_1.txt") |> Enum.count() |> IO.inspect(label: "total")
    Elf.q2("day_04/data/d4_1.txt") |> Enum.count() |> IO.inspect(label: "new total")
  