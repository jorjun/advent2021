defmodule Elf do

  Code.require_file("day_05/lib/data.ex")
  
  def q1(path, mover \\ &mover9000/1) do
    {crates, moves} =
    path
      |> Data.load()
      |> massage()
      
    new_crates = shuffle(moves, mover, crates)
        
    new_crates
      |> Map.values() 
      |> Enum.map(&List.first/1)
  end
  
  def q2(path),  do: q1(path, &mover9001/1)
  
  defp shuffle([], _, acc), do: acc
  defp shuffle([%{count: count, from: from, to: to}|tail], mover, acc) do
    {got, updated} = Map.get_and_update(acc, from, fn(c)-> Enum.split(c, count) end)
      
    new_acc = Map.get_and_update(updated, to, fn(c)-> {c, mover.(got) ++ c} end) 
                |> then(fn({_, n})-> n end)

    shuffle(tail, mover, new_acc)
  end
  
  defp mover9000(data),  do: Enum.reverse(data)
  defp mover9001(data),  do: data
    
  defp massage([head|[tail]]) do
    {index, data} = build_index(head)
    empty_stacks = 
    for n <- 1..Enum.count(index) do
      %{n => []}
    end
    |> Enum.reduce(&Map.merge/2)
    
    crates = build_crates(data, index, empty_stacks)
    moves = build_moves(tail)
    
    {crates, moves}
  end
  
  defp build_index(data) do
    {index, crates} =
    data
      |> String.split("\n")
      |> List.pop_at(-1)
      |> then(fn({i, d})-> {String.graphemes(i), d} end)
      
    len = 
      index
        |> Enum.reject(&(&1 == " "))
        |> Enum.count()
    
    new_index =
        for n <- 1..len do
          %{stack: n, offset: Enum.find_index(index, fn(v) -> v == to_string(n) end)}
        end  
    {new_index, crates}    
  end
  
  defp build_crates([], _, acc), do: acc
  defp build_crates([head|tail], index, acc) do
    
    new_acc = 
    index
      |> Enum.map(fn(%{stack: s, offset: o})-> {s, String.at(head, o)} end)
      |> Enum.reject(fn({_, v})-> v == " " end)
      |> update_crate_map(acc)
      
    build_crates(tail, index, new_acc)
  end
  
  defp update_crate_map([], acc), do: acc
  defp update_crate_map([{s,c}|tail], acc) do
    new_acc =
    acc
      |> Map.get_and_update(s, fn(v)-> {v, List.insert_at(v, -1, c)} end)
      |> then(fn({_, new})-> new end)
      
    update_crate_map(tail, new_acc)
  end
  
  defp build_moves(data) do
    data
      |> String.split("\n")
      |> Enum.map(&move_to_map/1)
  end
  
  defp move_to_map("move " <> <<count::utf8>> <> " from " <> <<from::utf8>> <> " to " <> <<to::utf8>>) do
    %{count: (count - 48), from: (from - 48), to: (to - 48)}
  end
  
  defp move_to_map("move " <> <<c1::utf8>> <> <<c2::utf8>> <> " from " <> <<from::utf8>> <> " to " <> <<to::utf8>>) do
    %{count: (((c1 - 48) * 10) + (c2 - 48)), from: (from - 48), to: (to - 48)}
  end
  
end

IO.puts("TEST")
    Elf.q1("day_05/data/test.txt") 
    |> Enum.join("") |> IO.inspect(label: "top")
    
    Elf.q2("day_05/data/test.txt")|> Enum.join("") |> IO.inspect(label: "new top")
      
IO.puts("\nACTUAL")
 #    # #
      Elf.q1("day_05/data/d5_1.txt") |> Enum.join("") |> IO.inspect(label: "top")
      Elf.q2("day_05/data/d5_1.txt") |> Enum.join("") |> IO.inspect(label: "new top")
  