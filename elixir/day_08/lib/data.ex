defmodule Data do
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&String.graphemes/1)
  end
  
  
end
