defmodule Elf do

  Code.require_file("day_08/lib/data.ex")
  
  @zero_based -1
  
  def at(grid, {x, y}),         do: Enum.at(grid, y) |> Enum.at(x)
  
  def north({x, y}),            do: {x, y - 1}
  def east({x, y}),             do: {x + 1, y}
  def south({x, y}),            do: {x, y + 1} 
  def west({x, y}),             do: {x - 1, y}
  
  def define_edges(%{max_x: mx, max_y: my} = data) do
    data
      |> Map.put(:north, Enum.map(1..(mx - 1), fn(x)-> {x , 0} end))
      |> Map.put(:south, Enum.map(1..(mx - 1), fn(x)-> {x, my} end))
      |> Map.put(:east,  Enum.map(1..(my - 1), fn(y)-> {mx, y} end))
      |> Map.put(:west,  Enum.map(1..(my - 1), fn(y)-> {0 , y} end))
  end
 
  def to_map(data) do
    width     = Enum.count(Enum.at(data, 0))
    height    = Enum.count(Enum.map(data, &Enum.at(&1, 0)))
    p         = (width * 2) + ((height - 2) * 2)
    %{perimeter: p, max_x: (width + @zero_based) , max_y: (height + @zero_based), grid: data}
  end
  
  def q1(path) do
    path
      |> Data.load()
      |> to_map()
      |> define_edges()
      |> walk()
  end
  
  def walk(%{perimeter: p} =data) do #NEVER EAT SHREDDED WHEAT
    [
      look(data, :from_north),
      look(data, :from_east),
      look(data, :from_south),
      look(data, :from_west),
    ]
    |> List.flatten()
    |> Enum.uniq()
    |> Enum.count()
    |> Kernel.+(p)
  end
  
  def look(%{north: n} = data, :from_north), do: look(n, data, &south/1,"", [])
  def look(%{east:  e} = data, :from_east),  do: look(e, data, &west/1, "", [])
  def look(%{south: s} = data, :from_south), do: look(s, data, &north/1, "", [])
  def look(%{west:  w} = data, :from_west),  do: look(w, data, &east/1, "", [])
  

  def look([], _, _, _, acc), do: acc
  def look([head|tail], %{grid: grid} = data, direction, edge, acc) do
    new_edge = if edge == "",  do: at(grid, head), else: edge
    new_acc = walk(head, new_edge,  data, direction,  acc)
    look(tail, data, direction, "", new_acc)
  end
  
  def walk(from, edge, %{grid: grid} = data, direction, acc) do
    nxt_pos = direction.(from)
    {min_x, max_x, min_y, max_y} = bounds(data, direction)
    case nxt_pos do
      {x , _} when x < min_x or x > max_x -> acc
      {_ , y} when y < min_y or y > max_y -> acc
      pos -> 
        case at(grid, pos) do
           val when val >  edge -> walk(pos, val, data, direction, List.insert_at(acc, -1, {pos, val}))
           val when val <  edge -> walk(pos, edge, data, direction, acc)
           val when val == edge -> walk(pos, edge, data, direction, acc)
           _  -> acc
         end
    end
  end
  
  def bounds(%{max_x: mx, max_y: my}, :north), do: {1, (mx - 1), 1, my}
  def bounds(%{max_x: mx, max_y: my}, :east),  do: {0, (mx - 1), 1, (my - 1)}
  def bounds(%{max_x: mx, max_y: my}, :south), do: {1, (mx - 1), 0, (my - 1)}
  def bounds(%{max_x: mx, max_y: my}, :west),  do: {1, mx, 1, (my - 1)}
  def bounds(data, direction) do 
    {_, dir} = Function.info(direction, :name)
    bounds(data, dir)
  end
  
  def q2(path) do
    path
      |> Data.load()
      |> to_map()
      |> walk2()
  end
  
  def look2(start, from, %{grid: grid, max_x: mx, max_y: my} = data, direction, acc) do
    to  = direction.(from)
    case to do
      {x , _} when x < 0 or x > mx -> acc
      {_ , y} when y < 0 or y > my -> acc
      pos -> 
        case at(grid, pos) do
        val when val >= start -> List.insert_at(acc, -1, {pos, val})
        val -> look2(start, to, data, direction, List.insert_at(acc, -1, {pos, val}))
      end
    end
  end
  
  def walk2(%{grid: grid, max_x: mx, max_y: my} = data) do
    for x <- 1..mx, y <- 1..my, into: [] do
      me  = at(grid, {x, y})
        {
          {x,y},
          [
            look2(me, {x, y}, data, &north/1, []) |> Enum.count(),
            look2(me, {x, y}, data, &east/1, [])  |> Enum.count(),
            look2(me, {x, y}, data, &south/1, []) |> Enum.count(),
            look2(me, {x, y}, data, &west/1, [])  |> Enum.count()
          ]
        }
    end
      |> Enum.map(fn({k, a})-> {k, Enum.reduce(a, 1, fn(v, acc)-> v * acc end)} end)
      |> Enum.max_by(fn({_,v})-> v end)
  end

end

IO.puts("TEST")
Elf.q1("day_08/data/test.txt") |> IO.inspect(label: "top")
Elf.q2("day_08/data/test.txt") |> IO.inspect(label: "new top")
# #
# IO.puts("\nACTUAL")
# #  #    # #
Elf.q1("day_08/data/d8_1.txt")|> IO.inspect(label: "new top", limit: :infinity)
Elf.q2("day_08/data/d8_1.txt")|> IO.inspect(label: "new top")