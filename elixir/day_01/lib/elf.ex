defmodule Elf do

  Code.require_file("day_01/lib/data.ex")
  
  def q1(path) do
    path
      |> Data.load()
      |> mapify()
      |> hd()
  end
  
  def q2(path) do
    path
      |> Data.load()
      |> mapify()
      |> Enum.take(3)
      |> IO.inspect(label: "highest 3")
      |> Enum.reduce(0, fn(%{"total"=> v},acc) -> acc + v end)
  end
  
  defp mapify(data) do
    data
      |> Enum.with_index(fn(e,i)-> %{"elf_#{i + 1}" => e, "total" => Enum.sum(e)} end)
      |> Enum.sort(fn(%{"total" => v1}, %{"total"=> v2}) -> v1 > v2 end)
  end
  
end

IO.puts("TEST")

Elf.q1("day_01/data/test.txt") |> IO.inspect(label: "highest")
Elf.q2("day_01/data/test.txt") |> IO.inspect(label: "highest '3' total")

IO.puts("ACTUAL")

Elf.q1("day_01/data/d1_1.txt") |> IO.inspect(label: "highest")
Elf.q2("day_01/data/d1_1.txt") |> IO.inspect(label: "highest '3' total")