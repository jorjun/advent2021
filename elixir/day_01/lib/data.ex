defmodule Data do
  
  def load(path) do
    path
      |> File.read!()
      |> String.split("\n\n")
      |> Enum.map(&String.split(&1,"\n"))
      |> Enum.map(&Enum.map(&1, fn(v) -> String.to_integer(v) end))
  end
  
end
