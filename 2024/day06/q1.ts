// @jorjun Vix ☉ : ♐ ☽ : ♍

interface Race {
  duration: number,
  distance: number
}

class Question {
  grid: string[][] = [[]]
  total = 1
  compass = [[0, -1], [1, 0], [0, 1], [-1, 0]]
  pos = { x: 0, y: 0 }
  current_dir = { i: 0, j: 0 }
  direction = this.navigate()

  constructor (private path: string) { }

  *navigate() {
    while (true) {
      for (const [i, j] of this.compass) yield { i, j }
    }
  }

  move() {
    const { x, y } = this.pos
    this.grid[y][x] = 'X'
    let [nx, ny] = [this.pos.x + this.current_dir.i, this.pos.y + this.current_dir.j]
    if (nx < 0 || nx >= this.grid[0].length || ny < 0 || ny >= this.grid.length) return { x: -1, y: -1 }
    const sq = this.grid[ny][nx]
    if (sq == "#") {
      this.current_dir = this.direction.next().value!;
      [nx, ny] = [this.pos.x + this.current_dir.i, this.pos.y + this.current_dir.j]
    }
    const mm = this.compass.map((dir, ix) => ({ ix, found: dir[0] == this.current_dir.i && dir[1] == this.current_dir.j })).filter(({ found }) => found)
    this.grid[ny][nx] = '^>v<'[mm[0].ix]
    return { x: nx, y: ny }
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.grid = _data.split("\n").filter(Boolean)
      .map((row, iy) => {
        const cols = row.split("")
        const sx = cols.indexOf("^")
        if (sx > -1) this.pos = { x: sx, y: iy }
        return cols
      })
    this.current_dir = this.direction.next().value!

    while (true) {
      const { x: nx, y: ny } = this.move()
      if (nx < 0 || nx > this.grid[0].length || ny < 0 || ny > this.grid.length) {
        break
      }
      this.pos = { x: nx, y: ny }
    }

    this.total = this.grid.reduce((count, row) => count += row.filter(col => col == "X").length, 0)

  }


}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(q.total)