// @jorjun Vix ☉ : ♐ ☽ : ♍

class Question {
  grid: string[][] = [[]]
  total = 0
  compass = [[0, -1], [1, 0], [0, 1], [-1, 0]]
  pos = { x: 0, y: 0 }
  start = { x: 0, y: 0 }

  constructor (private path: string) { }

  *navigate() {
    while (true) {
      for (const [i, j] of this.compass) yield { i, j }
    }
  }

  create2DArray<T>(width: number, height: number, fill: T): T[][] {
    return Array.from({ length: height }, () =>
      Array.from({ length: width }, (c) => fill)
    )
  }


  *move() {
    const direction = this.navigate()
    let current_dir = direction.next().value!
    let turn = 0
    while (true) {
      let [x, y] = [this.pos.x + current_dir.i, this.pos.y + current_dir.j]
      if (turn > 301) { // terrible
        yield { x: -1, y: -1, loop: true }
        break
      }
      if (x < 0 || x >= this.grid[0].length || y < 0 || y >= this.grid.length) {
        yield { x: -1, y: -1, loop: false }
        break
      }
      const sq = this.grid[y][x]
      if (sq == "#" || sq == "?") {
        current_dir = direction.next().value!
        turn++
        continue
      }
      yield { x, y }
      this.pos = { x, y }
    }
  }

  getTrail(obstacle: { x: number, y: number }) {
    const trailGrid = this.create2DArray<string>(this.grid[0].length, this.grid.length, '.')
    this.grid[obstacle.y][obstacle.x] = '?'
    for (const { ix, iy } of this.followTrail(this.grid, '#')) { trailGrid[iy][ix] = "#" }
    trailGrid[this.start.y][this.start.x] = '^'
    trailGrid[obstacle.y][obstacle.x] = '?'
    this.pos = this.start
    const move = this.move()
    while (true) {
      const { x: nx, y: ny, loop } = move.next().value!
      if (loop) {
        this.total++
        break
      }
      if (nx < 0) break
      const sq = trailGrid[ny][nx]
      if (sq !== '^')
        trailGrid[ny][nx] = "X"
    }
    this.grid[obstacle.y][obstacle.x] = '.'
    return trailGrid
  }

  * followTrail(trail: string[][], cmp: string) {
    for (let iy = 0; iy < trail.length; iy++) {
      for (let ix = 0; ix < trail[0].length; ix++) {
        if (trail[iy][ix] == cmp) yield { ix, iy }
      }
    }
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.grid = _data.split("\n").filter(Boolean)
      .map((row, iy) => {
        const cols = row.split("")
        const sx = cols.indexOf("^")
        if (sx > -1) this.start = { x: sx, y: iy }
        return cols
      })

    const initialTrail = this.getTrail({ x: 0, y: 0 })
    console.table(initialTrail)
    for (const { ix: x, iy: y } of this.followTrail(initialTrail, "X")) {
      const trail = this.getTrail({ x, y })
      trail[y][x] = '?'
      // console.table(trail)
    }
  }
}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)