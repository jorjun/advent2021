import * as fs from 'fs'
import * as sharp from 'https://deno.land/x/sharp/mod.ts'

async function createAnimation(folderPath: string, outputFilePath: string, frameDuration: number) {
  const imageFiles = await fs.readdir(folderPath)
  const images = await Promise.all(imageFiles.map((file) => sharp(`${folderPath}/${file}`)))

  await sharp({
    create: {
      width: images[0].width,
      height: images[0].height,
      channels: 4,
      background: { r: 0, g: 0, b: 0, alpha: 0 },
    },
  })
    .composite(images.map((image, index) => ({ input: image, top: 0, left: 0, blend: 'over' })))
    .gif({
      delay: frameDuration,
      loop: 0,
    })
    .toFile(outputFilePath)
}

// Example usage:
createAnimation('path/to/images', 'output.gif', 100)
