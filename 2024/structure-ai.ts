import ollama from 'npm:ollama'
import { z } from 'npm:zod'
import { zodToJsonSchema } from 'npm:zod-to-json-schema'

const parser = z.string().time()

const Country = z.object({
  from_country_iso_2: z.string(),
  from_city: z.string(),
  to_country_iso_2: z.string(),
  to_city: z.string(),
  from_date_yy_mm_dd: z.string(),
  from_time_hh_mm: z.string(),
})

const response = await ollama.chat({
  model: 'llama3.3',
  // messages: [{ role: 'user', content: 'current date is 2024 december 19. 12PM in abhu dhabi on july 12 what time is it in london?' }],
  messages: [{
    role: 'user', content: 'Current date is 2024 december 19. Time in Cairo, if 7PM in london?'
  }
  ],
  format: zodToJsonSchema(Country),
})

const convert = Country.parse(JSON.parse(response.message.content))
// for (const time of ['from_time_hh_mm', 'to_time_hh_mm']) {
// const tm = convert[time as keyof typeof convert] + ":00"
// console.log({ tm })
// convert[time as keyof typeof convert] = parser.parse(tm)
// }
console.log(convert)