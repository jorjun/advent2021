// @jorjun Vix ☉ : ♐ ☽ : ♍

interface Calibration {
  total: number
  reading: number[]
}


class Question {
  data: Calibration[] = []
  total = 0

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n").filter(Boolean).map(line => {
      const [_key, _vals] = line.split(":")
      const total = Number(_key), reading = _vals.trim().split(' ').map(Number)
      return {
        total, reading
      }
    })
    for (const { total, reading } of this.data) {
      for (const ops of this.getOperands(reading.length - 1)) {
        const sum = this.calculate(ops, reading)
        if (sum == total) {
          this.total += sum
          break
        }
      }
    }
  }

  calculate(ops: string[], reading: number[]) {
    const sum = reading.reduce((acc, num, ix) => {
      if (ix == 0) return num
      if (ops[ix - 1] == "1") return acc * num
      else return acc + num
    }, 0)
    return sum
  }

  *getOperands(num: number) {
    for (let ix = 0; ix < 2 ** num; ix++) {
      const bin = ix.toString(2).padStart(num, '0').split("")
      yield bin
    }
  }
}

const q = new Question("./test.txt")
// const q = new Question("./data.txt")
await q.load()
console.log(q.total)
