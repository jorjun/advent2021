// @jorjun 

export class Question {
  total = 0
  seq: number[] = []

  constructor (private path: string) { }

  blink(stones: () => Generator<number>) {
    function* nums() {
      for (const num of stones()) {
        if (num == 0) {
          yield 1
          continue
        }

        // Even digits?
        const st = num.toString()
        const len = st.length
        if (len % 2 == 0) {
          yield Number(st.slice(0, len / 2))
          yield Number(st.slice(len / 2))
          continue
        }

        yield 2024 * num
      }
    }
    return nums
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    function* stones(): Generator<number> {
      for (const num of _data.split(" ").map(Number))
        yield num
    }

    let result = stones, total = 0
    for (let ix = 1; ix <= 15; ix++) {
      total = 0
      result = this.blink(result)
    }
    for (const num of result()) {
      this.seq.push(num)
      total++
    }

  }
}

// const q = new Question("./test.txt")
// const q = new Question("./test2.txt")
const q = new Question("./src/lib/day11/data.txt")
// await q.load()
// console.log(q.total)
