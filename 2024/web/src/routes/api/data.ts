import type { RequestHandler } from '@sveltejs/kit'
import { json } from '@sveltejs/kit'


export const GET: RequestHandler = async () => {


  // const key = url.searchParams.get('key')

  const data = await Deno.readTextFile('./test.txt')

  return json({ key, data }) // Use json() to return a Response

  // return json({ error: "Key not found" }, { status: 404 }) // Return a 404 response
}