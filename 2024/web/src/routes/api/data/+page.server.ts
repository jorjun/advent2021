// import type { RequestHandler } from '@sveltejs/kit'


import type { PageServerLoad } from './$types'

export const load: PageServerLoad = async ({ params }) => {
  console.log("Current dir", Deno.cwd())
  return {
    data: await Deno.readTextFile('./data/day14/test.txt')
  }
}

