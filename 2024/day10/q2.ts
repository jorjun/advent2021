// @jorjun Vix ☉ : ♐ ☽ : ♍

class Question {
  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const data = _data.split("\n").filter(_ => _).map(n => n.split(" ").map(_ => Number(_)))
    const grid: Record<number, number[][]> = [], delta = []
    data.forEach((row, ix) => {
      grid[ix] = [row]
      let diff = row
      for (; ;) {
        diff = this.get_diff(diff)
        grid[ix].push(diff)
        if (diff.filter((_) => _ == 0).length === diff.length) break
      }
      const g_row = grid[ix]
      const rr = g_row.slice(1).map(_ => _[0]).reduce((sum, cur) => sum + cur, 0)
      delta.push(rr)
    })
    console.log({ grid, delta })
    let sum = 0
    grid.forEach((gg, ix) => {
      sum += gg[0].at(0) + delta[ix]
    })
    console.log({ sum })
  }

  get_diff<T>(arr: T[]) {
    function* _pair_iter(arr: T) {
      for (let ix = 0; ix < arr.length - 1; ix++) yield [arr[ix + 1], arr[ix]]
    }
    return Array.from(_pair_iter(arr)).map(([i, j]) => j - i)
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
