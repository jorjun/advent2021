// @jorjun 

interface Cell {
  x: number,
  y: number,
  num: number
}

const directions = [
  [0, 1],  // right
  [1, 0],  // down
  [0, -1], // left
  [-1, 0]  // up
]

class Question {
  grid: number[][] = []
  visited: boolean[][] = []
  pathList: Cell[][] = [];
  total = 0

  constructor (private path: string) { }

  inBounds(x: number, y: number): boolean {
    const { grid, visited } = this
    return x >= 0 && x < grid.length && y >= 0 && y < grid[0].length && !visited[y][x]
  }

  search(cell: Cell, path: Cell[]) {
    const { visited, grid, pathList } = this

    path.push(cell)
    const { x, y, num } = cell
    visited[y][x] = true

    if (grid[y][x] === 9) {
      pathList.push([...path]) // Found a path, store a path-copy
    } else {
      for (const [nx, ny] of directions.map(([i, j]) => [x + i, y + j])) {
        if (!this.inBounds(nx, ny)) continue
        if (grid[ny][nx] == num + 1)
          this.search({ x: nx, y: ny, num: num + 1 }, path)
      }
    }
    // Backtrack
    path.pop()
    visited[y][x] = false
  }


  gridFind(cmp: number) {
    const { grid } = this, height = grid.length, width = grid[0].length
    return Array.from(_gridFind())
    function* _gridFind() {
      for (let iy = 0; iy < height; iy++) {
        for (let ix = 0; ix < width; ix++) {
          const num = Number(grid[iy][ix])
          if (num == cmp) yield { x: ix, y: iy, num }
        }
      }
    }
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.grid = _data.split("\n").filter(Boolean).map(row => row.split("").map(c => c == "." ? -1 : Number(c)))
    const { grid } = this, height = grid.length, width = grid[0].length
    this.visited = Array.from({ length: height }, () => Array.from({ length: width }, () => false))
    const score: Record<string, number> = {}
    for (const cell of this.gridFind(0)) {
      this.search(cell, [])
      this.total += this.pathList.length
      this.pathList = []
      this.visited = Array.from({ length: height }, () => Array.from({ length: width }, () => false))
    }
    console.log(score)
  }

}

// const q = new Question("./test3.txt")
// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)
