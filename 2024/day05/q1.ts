// @jorjun Vix ☉ : ♐ ☽ : ♍

class Question {
  page_order: Record<string, string[]> = {}
  page_update: string[] = []
  total = 0

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const dd = _data.split("\n\n").map(m => m.split('\n'))
    for (const order of dd[0]) {
      const [ky, val] = order.split('|')
      if (!this.page_order[ky])
        this.page_order[ky] = []
      this.page_order[ky].push(val)

    }
    this.page_update = dd[1]
    for (const upd of this.page_update) {
      const updateList = upd.split(",")
      const update_len = updateList.length, mid = Math.floor(updateList.length / 2)
      let good = true
      for (let ix = 0; ix < update_len - 1; ix++) {
        const [p1, p2] = [updateList[ix], updateList[ix + 1]]
        const [order1, order2] = [this.page_order[p1], this.page_order[p2]]
        if (order1 && !order1.includes(p2)) {
          good = false
          break
        }
        if (order2 && order2.includes(p1)) {
          good = false
          break
        }
      }
      if (good) {
        this.total += Number(updateList[mid])
      }
    }
    // console.log(this.page_update)
  }

}

// const q = new Question("./test/input")
const q = new Question("./input")
await q.load()
console.log(q.total)