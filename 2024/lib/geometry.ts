export interface Point {
  x: number
  y: number
}

export const directions = [
  [0, -1], // north
  [1, 0],  // east
  [0, 1],  // south
  [-1, 0], // west
]

export const directions8 = [
  [0, -1], // north
  [1, -1], // N east
  [1, 0],  // east
  [1, 1],  // S east
  [0, 1],  // south
  [-1, -1],// N west
  [-1, 0], // west
  [-1, 1], // S west
]

export interface Cell extends Point {
  attr: string
}

export function orientation(p1: Point, p2: Point, p3: Point): number {
  const val = (p2.y - p1.y) * (p3.x - p2.x) - (p2.x - p1.x) * (p3.y - p2.y)
  return val == 0 ? 0 : val > 0 ? 1 : 2
}

export function convexHull(points: Point[]): Point[] {
  if (points.length < 3) {
    throw new Error("Convex hull not possible with less than 3 points")
  }

  // Leftmost point
  let start = points[0]
  for (const point of points.slice(1)) {
    if (point.x < start.x) start = point
  }

  // Sort by polar angle with start
  points.sort((p1, p2) => {
    const orient = orientation(start, p1, p2)
    if (orient === 0) {
      // If collinear, sort by distance from start point
      const dA = Math.hypot(p1.x - start.x, p1.y - start.y)
      const dB = Math.hypot(p2.x - start.x, p2.y - start.y)
      return dA - dB
    }
    return orient - 1
  })

  const hull: Point[] = [points[0], points[1]]
  for (let i = 2; i < points.length; i++) {
    while (hull.length > 1 && orientation(hull.at(-2)!, hull.at(-1)!, points[i]) !== 2) {
      hull.pop()
    }
    hull.push(points[i])
  }

  // Close the hull by adding the first point again
  hull.push(hull[0])

  // Remove any points that are inside the hull
  const outerPoints: Point[] = Array.from(outerHull())
  function* outerHull() {
    for (let ix = 0; ix < hull.length - 1; ix++) {
      const [p1, p2, p3] = [hull[ix], hull[ix + 1], hull[(ix + 2) % hull.length]]
      if (orientation(p1, p2, p3) === 2) yield p1
    }
  }
  return outerPoints
}


export class Grid {
  grid: Cell[][]
  width = 0
  height = 0

  constructor (width: number, height: number, fill = ".") {
    this.grid = Array(height).fill(0).map((_, y) => {
      return Array(width).fill(0).map((_, x) => ({ x, y, attr: fill }))
    })
    this.width = this.grid[0].length
    this.height = this.grid.length
  }
  inBounds(x: number, y: number) {
    const { width, height } = this
    return (x >= 0 && x < width && y >= 0 && y < height)
  }

  static async load(path: string) {
    const _data = await Deno.readTextFile(path)
    const data = _data.split("\n").filter(Boolean)
      .map((row, y) =>
        row.split("").map((attr, x) => ({ x, y, attr })))
    const ng = new Grid(data[0].length, data.length)
    ng.grid = data
    return ng
  }

  static fromList(points: Cell[]) {
    const rr = Math.max(...points.map(({ x }) => x)),
      ll = Math.min(...points.map(({ x }) => x)), size = rr - ll + 3
    const ng = new Grid(size, size)
    for (const { x, y, attr } of points) {
      if (ng.inBounds(x + 1, y + 2)) {
        ng.grid[y + 2][x + 1] = { attr, x, y }
      }
    }
    return ng
  }

  perimeter(region: Cell[]) {
    const self = this
    return Array.from(_get())

    function* _get() {
      const inRegion = (x: number, y: number) =>
        region.some(({ x: rx, y: ry }) => rx == x && ry == y)
      let adj = 0
      for (const { x, y } of region) {
        for (const [nx, ny] of directions.map(([i, j]) => [x + i, y + j])) {
          if (inRegion(nx, ny)) continue
          if (!self.inBounds(nx, ny)) {
            yield { x: nx, y: ny, attr: '#' }
            continue
          }
          yield { x: nx, y: ny, attr: '#' }
        }
      }
    }
  }

  gridFind(crit: (cell: Cell) => boolean) {
    const { grid, width, height } = this
    return Array.from(_gridFind())

    function* _gridFind() {
      for (let iy = 0; iy < height; iy++) {
        for (let ix = 0; ix < width; ix++) {
          const cell = grid[iy][ix]
          if (crit(cell)) yield cell
        }
      }
    }
  }

  print() {
    for (const row of this.grid) {
      console.log(row.map(cell => cell.attr).join(''))
    }

  }

}

