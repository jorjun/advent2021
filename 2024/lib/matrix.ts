// @jorjun Vx ☉ : ♑ ☽ : ♎

export const determinant = ([a, b]: number[][]) =>
  a[0] * b[1] - a[1] * b[0]

export function cramerRule([a, b]: number[][], p: number[]): number[] {
  const D = determinant([a, b])
  if (D == 0) return [0, 0]

  const detX = determinant([p, b]), detY = determinant([a, p])
  return [detX / D, detY / D]
}

// example
// const matrix = [
//   [94, 34],
//   [22, 67],
// ]
// const sol = cramerRule(matrix, [8400, 5400]) // x: 40, y: 80
// console.log(sol)