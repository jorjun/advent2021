// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Coordinate {
  x: number, y: number
}

const Compass = [[1, 1], [-1, -1], [1, -1], [-1, 1]]
console.log({ Compass })
class Question {
  grid: string[][] = [[]]
  gridHeight = 0
  gridWidth = 0
  total = 0
  searchTerm = "MAS"

  constructor (private path: string) { }

  create2DArray(): any[][] {
    return Array.from({ length: this.gridHeight }, () =>
      Array.from({ length: this.gridWidth }, () => "")
    )
  }

  gridPath(co: Coordinate) {
    const { searchTerm } = this
    return Array.from(_genGridPath(this))

    function* _genGridPath(self: Question) {
      for (const [i, j] of Compass) {
        let { x, y } = co
        let path = self.grid[y][x]
        for (const lett of searchTerm.split('').slice(1)) {
          x += i; y += j
          if (x < 0 || x >= self.gridWidth || y < 0 || y >= self.gridHeight) break
          const cell = self.grid[y][x]
          if (lett == cell)
            path += lett
          else break
        }
        if (path == searchTerm) yield { path, x: co.x, y: co.y, i, j }
      }
    }
  }

  async load() {
    const data = await Deno.readTextFile(this.path)
    const rowData = data.split("\n").filter(Boolean)
    this.gridHeight = rowData.length, this.gridWidth = rowData[0].length
    this.grid = this.create2DArray()

    rowData.map((row, iy) =>
      row.split("").map((cell, ix) => {
        this.grid[iy][ix] = cell
      })
    )

    // Try each starting point
    const mas_list = []
    for (let iy = 0; iy < this.gridHeight; iy++) {
      for (let ix = 0; ix < this.gridWidth; ix++) {
        for (const word of this.gridPath({ x: ix, y: iy })) {
          mas_list.push(word)
        }
      }
    }
    const crossList = mas_list.map(w => `${w.x + w.i}:${w.y + w.j}`).toSorted()
    for (let ix = 0; ix < crossList.length - 1; ix++) {
      if (crossList[ix] == crossList[ix + 1]) {
        this.total++
      }
    }

  }
}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
console.log(new Date())
await q.load()
console.log(
  q.total
)
// 1945
