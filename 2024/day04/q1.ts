// @jorjun Vix ☉ : ♐ ☽ : ♌

interface Coordinate {
  x: number, y: number
}

const Compass = [1, 2, 3, 4, 5, 6, 7, 8].map(x => x.toString(3).padStart(2, '0').split('').map(n => n == '2' ? '-1' : n).map(Number))

class Question {
  rowData: string[] = []
  gridHeight = 0
  gridWidth = 0
  total = 0
  searchTerm = "XMAS"

  constructor (private path: string) { }

  *gridPath(co: Coordinate) {
    const { searchTerm, gridWidth, gridHeight, rowData } = this
    for (const [i, j] of Compass) {
      let path = rowData[co.y][co.x]
      for (let p = 1; p < searchTerm.length; p++) {
        const x = co.x + i * p, y = co.y + j * p
        if (x < 0 || x >= gridWidth || y < 0 || y >= gridHeight) break
        const cell = rowData[y][x]
        if (searchTerm[p] == cell)
          path += cell
        else break
      }
      if (path == searchTerm) yield true
    }
  }

  async load() {
    const data = await Deno.readTextFile(this.path)
    this.rowData = data.split("\n").filter(Boolean)
    this.gridHeight = this.rowData.length,
      this.gridWidth = this.rowData[0].length

    // Scan directions from every grid pos. start
    for (let iy = 0; iy < this.gridHeight; iy++) {
      for (let ix = 0; ix < this.gridWidth; ix++)
        this.total += Array.from(this.gridPath({ x: ix, y: iy })).length
    }

  }
}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.load()
console.log(
  q.total
)
