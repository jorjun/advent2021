// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  total = 0

  constructor (private path: string) { }

  async load() {
    const data = await Deno.readTextFile(this.path)

    this.total = Array.from(data
      .replaceAll(/don't\(\).*?(?:do\(\)|$)/gs, "") // replace [don't -> (do() or eof)] with "" 
      .matchAll(/mul\((\d{1,3}),(\d{1,3})\)/g))     // two groups for digits x2
      .map(match => match.slice(1, 3).map(Number))  // match[1] & match[2] mapped to numbers
      .reduce((sum, [a, b]) => sum + a * b, 0)
  }
}

// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
console.log(
  q.total
)
if (q.total !== 70478672) throw 'bad'
