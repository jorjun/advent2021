// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  total = 0

  constructor (private path: string) { }

  async load() {
    this.data = await Deno.readTextFile(this.path)
    for (const { a, b } of this.parse()) {
      this.total += a * b
    }
  }

  parse() {
    return [
      ...this.data.matchAll(/mul\((\d{1,3}),(\d{1,3})\)/g)
        .map((match) => {
          return { a: Number(match[1]), b: Number(match[2]) }
        })
    ]
  }

}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.load()
console.log(
  q.total
)
