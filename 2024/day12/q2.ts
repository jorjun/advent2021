// @jorjun 

import { Grid, Cell, directions, directions8 } from "../lib/geometry.ts"

// 

class Question {
  total = 0
  garden!: Grid
  visited!: Grid

  constructor (private path: string) { }

  async load() {
    this.garden = await Grid.load(this.path)
    const { garden } = this, height = garden.height, width = garden.width
    this.visited = new Grid(width, height, " ")
    const plantList = garden.gridFind(({ attr }: Cell) => attr == "I")

    let region: Cell[] = []
    for (const cell of plantList) {
      region = this.getRegion(cell)
      if (region.length > 0) {
        const poly = garden.perimeter(region)
        const pgrid = Grid.fromList(poly)
        const sides = this.countSides(pgrid)
        const rgrid = Grid.fromList(region)
        rgrid.print()
        console.log(cell, region)
        console.log("=====================")
        this.total += sides * region.length
      }
    }
  }

  // Adjacent cells
  getRegion({ x, y, attr }: Cell) {
    const region: Cell[] = [], { garden, visited } = this, { grid } = garden
    dfs({ x, y, attr })
    return region

    function dfs({ x, y, attr: target }: Cell) {
      if (!garden.inBounds(x, y) || grid[y][x].attr !== target || visited.grid[y][x].attr == target)
        return
      region.push({ x, y, attr: target })
      visited.grid[y][x].attr = target

      for (const [i, j] of directions) {
        dfs({ x: x + i, y: y + j, attr: target })
      };
    }
  }


  countSides(region: Grid): number {
    const visited: Set<string> = new Set(), { grid } = region
    let sides = 0

    for (const cell of region.gridFind(({ attr }) => attr == "#")) {
      if (isCorner(cell)) sides++
    }
    return sides

    function isCorner(cell: Cell) {
      let bound = 0; const { x, y } = cell
      for (const [i, j] of directions) {
        const [nx, ny] = [x + i, y + j]
        if (!region.inBounds(nx, ny)) continue
        if (grid[ny][nx].attr == ".") bound++
      };
      return bound == 2
    }

  }
}

// const q = new Question("./test1.txt")
const q = new Question("./test2.txt")
// const q = new Question("./test4.txt")
// const q = new Question("./data.txt")
await q.load()
console.log(q.total)
