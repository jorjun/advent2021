// @jorjun 

interface Cell {
  x: number,
  y: number,
  plant: string
}

// 
const directions = [
  [0, -1], // north
  [1, 0],  // east
  [0, 1],  // south
  [-1, 0], // west
]

class Question {
  total = 0
  grid: string[][] = []
  visited: string[][] = []
  region: Cell[] = []

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)

    this.grid = _data.split("\n").filter(Boolean).map((row) => row.split(""))
    const { grid } = this, height = grid.length, width = grid[0].length
    this.visited = Array.from({ length: height }, () => Array.from({ length: width }, () => ""))
    const plantList = this.gridFind((plant: string) => plant !== "")

    const regionList: Cell[][] = []
    for (const cell of plantList) {
      if (this.region.filter(({ x, y }) => x == cell.x || y == cell.y).length > 0) continue
      this.fill(cell, cell.plant)
      if (this.region.length > 0)
        regionList.push(this.region)
      this.region = []
    }
    for (const region of regionList) {
      console.table(region)
      this.total += region.length * this.perimeter(region)
    }
  }

  inBounds(x: number, y: number): boolean {
    const { grid } = this
    return x >= 0 && x < grid.length && y >= 0 && y < grid[0].length
  }

  fill({ x, y, plant }: Cell, target: string): void {
    const { grid, region, visited } = this
    if (!this.inBounds(x, y) || grid[y][x] !== target || visited[y][x] == target) return

    region.push({ x, y, plant })
    visited[y][x] = plant

    for (const [nx, ny] of directions.map(([i, j]) => [x + i, y + j])) {
      this.fill({ x: nx, y: ny, plant }, target)
    };
  }

  perimeter(region: Cell[]) {
    const inRegion = (x: number, y: number) =>
      region.some(({ x: rx, y: ry }) => rx == x && ry == y)
    let adj = 0
    for (const { x, y } of region) {
      for (const [nx, ny] of directions.map(([i, j]) => [x + i, y + j])) {
        if (inRegion(nx, ny)) continue
        if (!this.inBounds(nx, ny)) {
          adj++
          continue
        }
        adj++
      }
    }
    return adj
  }


  gridFind(crit: (plant: string) => boolean) {
    const { grid } = this, height = grid.length, width = grid[0].length
    return Array.from(_gridFind())
    function* _gridFind() {
      for (let iy = 0; iy < height; iy++) {
        for (let ix = 0; ix < width; ix++) {
          const plant = grid[iy][ix]
          if (crit(plant)) yield { x: ix, y: iy, plant }
        }
      }
    }
  }

}


// const q = new Question("./test.txt")
const q = new Question("./test2.txt")
// const q = new Question("./data.txt")
await q.load()
console.log(q.total)
