// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  total = 0

  constructor (private path = "./d1.txt") { }

  async run() {
    this.data = await Deno.readTextFile(this.path)

    for (const line of this.data.split("\n").filter(Boolean)) {
      const exp = Array.from((line.matchAll(/\d+/g).map(num => Number(num))))
      const delta = exp.slice(1).map((r, ix) => {
        const l = exp[ix]
        const diff = r - l, sgn = Math.sign(diff)
        return { diff, sgn }
      })
      const diff = delta.map(r => r.diff), sgn = delta.map(r => r.sgn)
      const t1 = diff.every((num) => num !== 0 && Math.abs(num) < 4)
      const t2 = sgn.every((d) => d !== 0 && d == sgn[0])
      if (t1 && t2) this.total++
    }
  }

}


// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.run()
console.log(
  q.total
)
