// @jorjun Vix ☉ : ♐ ☽ : ♌

class Question {
  data = ""
  total = 0

  constructor (private path = "./d1.txt") { }

  getRow(arr: number[]) {
    return arr.slice(1).map((r, ix) => {
      const diff = r - arr[ix]
      return { diff: Math.abs(diff), sgn: Math.sign(diff) }
    })
  }

  async run() {
    this.data = await Deno.readTextFile(this.path)

    for (const line of this.data.split("\n").filter(Boolean)) {
      const exp = Array.from((line.matchAll(/\d+/g).map(num => Number(num))))

      if (this.valid_row(exp)) {
        this.total++
        continue
      }
      // Single element tolerance:
      let valid = false
      for (let ix = 0; ix < exp.length; ix++) {
        const nexp = [...exp.slice(0, ix), ...exp.slice(ix + 1)]
        valid = this.valid_row(nexp)
        if (valid)
          break
      }
      if (valid) this.total++
    }
  }

  valid_row(inp: number[]) {
    const row = this.getRow(inp)
    const diff = row.map(r => r.diff), sgn = row.map(r => r.sgn)
    const t1 = diff.every((num) => num !== 0 && num < 4)
    const t2 = sgn.every((d) => d !== 0 && d == sgn[0])
    return t1 && t2
  }

}

// const q = new Question("./test.txt")
const q = new Question("./data.txt")
await q.run()
console.log(
  q.total
)
