// @jorjun Vix ☉ : ♐ ☽ : ♍


class Question {
  data: string[][] = []

  constructor (private path: string) { }

  scan() {
    const { data } = this, height = data.length, width = data[0].length
    return Array.from(_scan())
    function* _scan() {
      for (let y = 0; y < height; y++)
        for (let x = 0; x < width; x++) {
          const cell = data[y][x]
          if (cell != '.' && cell != "#") yield { y, x, cell }
        }
    }
  }

  antinode(cmp: string) {
    const { data } = this, height = data.length, width = data[0].length
    const nodeList = this.scan().filter(({ cell }) => cell == cmp)
    return Array.from(_iter())
    function* _iter() {
      for (const a of nodeList) {
        for (const b of nodeList) {
          if (a == b) continue
          const { x: x1, y: y1, cell } = a,
            { x: x2, y: y2 } = b,
            { x, y } = { x: x2 * 2 - x1, y: y2 * 2 - y1 }
          if (x >= 0 && x < width && y >= 0 && y < height)
            yield { x, y, cell }
        }
      }
    }
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n").filter(Boolean).map(row => row.split(""))

    const count: Record<string, number> = {}
    const set = new Set<string>()

    for (const { cell } of this.scan()) {
      if (count[cell]) continue
      count[cell] = 1
      const antinode = this.antinode(cell)
      for (const { x, y } of antinode) {
        set.add(`${x},${y}`) // String unique
      }
    }
    console.log(set.size)
  }
}

// const q = new Question("./test.txt")
// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
