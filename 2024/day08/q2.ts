// @jorjun Vx ☉ : ♐ ☽ : ♈
enum Search {
  node,
  empty,
  filled
}

class Question {
  data: string[][] = []
  total = 0

  constructor (private path: string) { }

  scan(kind: Search) {
    const { data } = this, height = data.length, width = data[0].length
    return Array.from(_scan())
    function* _scan() {
      for (let y = 0; y < height; y++)
        for (let x = 0; x < width; x++) {
          const cell = data[y][x]
          if (kind == Search.node) {
            if (cell != "." && cell !== "#") yield { y, x, cell }
          }
          else if (kind == Search.empty) {
            if (cell == ".") yield { y, x, cell }
          }
          else if (kind == Search.filled) {
            if (cell !== ".") yield { y, x, cell }
          }
        }
    }
  }

  antinode(cmp: string) {
    const { data } = this, height = data.length, width = data[0].length
    const self = this
    const nodeList = this.scan(Search.node).filter(({ cell }) => cell == cmp)
    return Array.from(_iter())
    function* _iter() {
      for (const a of nodeList) {
        for (const b of nodeList) {
          if (a.x == b.x && a.y == b.y) continue
          for (let pos of self.scan(Search.empty)) {
            const { x: x1, y: y1 } = a,
              { x: x2, y: y2 } = b,
              { x: x3, y: y3, cell } = pos

            const collinear = () => x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2) == 0
            if (collinear()) {
              self.data[y3][x3] = "#"
              yield { x: x3, y: y3, cell: '#' }
            }
          }
        }
      }
    }
  }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    this.data = _data.split("\n").filter(Boolean).map(row => row.split(""))

    const count: Record<string, number> = {}

    for (const { cell } of this.scan(Search.node)) {
      if (count[cell]) continue // Once scan per frequency

      count[cell] = 1
      const _antinode = this.antinode(cell)
    }
    this.total = this.scan(Search.filled).length
  }
}

// const q = new Question("./test.txt")
// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
// console.table(q.data)
console.log(q.total)
// Answer : 1229