// @jorjun Vx ☉ : ♐ ☽ : ♐



class Question {
  left: number[] = []
  right: Record<number, number> = {}
  total = 0

  constructor (private path = "./d1.txt") { }

  async load() {
    const input = await Deno.readTextFile(this.path)
    const data = input.split("\n").filter(r => r > "").map((row) => {
      const data = Array.from(row.matchAll(/\d+/g), (match) => match[0]).map(Number)
      const [left, right] = data
      return { left, right }
    })

    for (const { left, right } of data) {
      this.left.push(left)
      if (this.right[right]) this.right[right]++
      else this.right[right] = 1
    }
  }

  run() {
    const left = this.left.toSorted()
    const right = this.right
    for (let ix = 0; ix < left.length; ix++) {
      const num = left[ix]
      this.total += num * (right[num] ?? 0)
    }
  }

  output() {
    console.log(this.total)
  }
}

// const q = new Question("./t1.txt")
const q = new Question()
await q.load()
q.run()
console.log(
  q.output()
)
