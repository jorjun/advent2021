// @jorjun Vx ☉ : ♐ ☽ : ♐



class Question {
  left: number[] = []
  right: number[] = []
  total = 0

  constructor (private path = "./d1.txt") { }

  async load() {
    const input = await Deno.readTextFile(this.path)
    const data = input.split("\n").filter(r => r > "").map((row) => {
      const data = Array.from(row.matchAll(/\d+/g), (match) => match[0]).map(Number)
      const [left, right] = data
      return { left, right }
    })

    for (const { left, right } of data) {
      this.left.push(left)
      this.right.push(right)
    }
  }

  run() {
    const left = this.left.toSorted()
    const right = this.right.toSorted()
    for (let ix = 0; ix < left.length; ix++) {
      const [l, r] = [left[ix], right[ix]]
      const diff = l - r
      console.log(l, r, diff)
      this.total += Math.abs(diff)
    }
  }

  output() {
    console.log(this.total)
  }
}

// const q = new Question("./t1.txt")
const q = new Question()
await q.load()
q.run()
console.log(
  q.output()
)
