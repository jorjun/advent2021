// @jorjun 

const cache: { [key: string]: number } = {}

class Question {
  total = 0

  constructor (private path: string) { }

  blink(num: number, count: number): number {
    if (count == 0) {
      return 1
    }

    let res = cache[`${num}:${count}`]  // CACHE MUST contain the count !!
    if (res)
      return res

    if (num == 0) {
      res = this.blink(1, count - 1)
    }
    else {
      // Even digits?
      const st = num.toString()
      const len = st.length
      if (len % 2 == 0) {
        const num1 = Number(st.slice(0, len / 2))
        const num2 = Number(st.slice(len / 2))
        res = this.blink(num1, count - 1) + this.blink(num2, count - 1)
      }
      else
        res = this.blink(2024 * num, count - 1)
    }
    cache[`${num}:${count}`] = res
    return res
  }


  async load() {
    const _data = await Deno.readTextFile(this.path)
    let count = 75, sum = 0
    for (const num of _data.split(" ").map(Number)) {
      sum = this.blink(num, count)
      this.total += sum
    }
  }
}


// const q = new Question("./test.txt")
// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)
