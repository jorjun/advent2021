// @jorjun Vix ☉ : ♐ ☽ : ♍

class Question {
  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const disk = _data.split("").filter(Boolean).map(Number)
      .filter((_, ix) => ix % 2 == 0).map((cur, ix) =>
        `${ix}`.repeat(cur)
      )

    const free = _data.split("").filter(Boolean).map(Number)
      .filter((_, ix) => ix % 2 == 1)
      .map((cur) =>
        ".".repeat(cur)
      ).flat()

    console.table(disk.join(""), free)

    while (true) {
      const lhs = disk.indexOf(".")
      const rhs = disk.findLastIndex((p) => p != ".")
      if (rhs <= lhs)
        break
      disk[lhs] = disk[rhs]
      disk[rhs] = "."
    }

    console.table(disk.join(""))

    // console.log(disk.filter(c => c != ".").reduce((prv, cur, ix) => {
    //   return prv + Number(cur) * ix
    // }, 0))
  }

}

const q = new Question("./test.txt")
// const q = new Question("./test2.txt")
// const q = new Question("./data.txt")
await q.load()

