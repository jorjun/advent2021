// @jorjun 

// Backtracking idiocy, but it did work 
// Didn't know about Cramer's rule, or that the cost minimisation was red herring. Doh. 
// Was going to plot, which would have revealed the straight line to the prize.. +1 for extra visualisation effort.. 

function mod(a: number, b: number): number {
  if (b === 0) {
    throw new Error("Division by zero is not allowed.")
  }
  return ((a % b) + b) % b
}

interface Robot {
  seq: number,
  p: { x: number, y: number }
  v: { i: number, j: number }
}

class Question {
  robots: Robot[] = []
  total = 0

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const lines = _data.split("\n")
    this.robots = Array.from(parse())
    // this.play(100, 11, 7)
    this.play(100, 101, 103)

    function* parse() {
      let seq = 0
      for (const mac of lines) {
        const item = Array.from(mac.matchAll(/p=(-?\d{1,3}),(-?\d{1,3})\sv=(-?\d{1,3}),(-?\d{1,3})/g))
          .map((match) => {
            return {
              seq: seq++,
              p: {
                x: Number(match[1]), y: Number(match[2]),
              },
              v: { i: Number(match[3]), j: Number(match[4]) }
            }
          })
        if (item[0])
          yield item[0]
      }
    }
  }

  play(num: number, width: number, height: number) {
    const midX = Math.floor(width / 2), midY = Math.floor(height / 2)
    const quandrant = [0, 0, 0, 0]
    const result: any[] = []
    for (const robot of this.robots) {
      const { p, v, seq } = robot
      // console.log(this.robots[seq])
      robot.p = { x: mod((p.x + num * v.i), width), y: mod(p.y + num * v.j, height) }
      const { x, y } = robot.p
      if (x == midX || y == midY) continue
      let quadX = x < midX ? 0 : 1, quadY = y < midY ? 0 : 1
      const quad = quadX + 2 * quadY
      quandrant[quad]++
    }
    console.log(quandrant)
    console.log(quandrant.reduce((acc, cur) => acc * cur, 1))
  }
  // console.log(result.reduce((acc, cost) => acc + cost, 0)) // 29388
}


// const q = new Question("./test1.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)
