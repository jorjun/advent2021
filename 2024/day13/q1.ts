// @jorjun 

// Backtracking idiocy, but it did work 
// Didn't know about Cramer's rule, or that the cost minimisation was red herring. Doh. 
// Was going to plot, which would have revealed the straight line to the prize.. +1 for extra visualisation effort.. 


interface Claw {
  seq: number
  button: { a: number[], b: number[] },
  prize: number[]
}

class Question {
  arcade: Claw[] = []
  total = 0

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const section = _data.split("\n\n")
    this.arcade = Array.from(parse())
    this.play()

    function* parse() {
      let seq = 0
      for (const mac of section) {
        const item = Array.from(mac.matchAll(/Button A: X\+(\d{1,2}), Y\+(\d{1,2})\sButton B: X\+(\d{1,2}), Y\+(\d{1,2})\sPrize: X=(\d{1,5}), Y=(\d{1,5})/gm))
          .map((match) => {
            return {
              seq: seq++,
              button: { a: [Number(match[1]), Number(match[2])], b: [Number(match[3]), Number(match[4])] },
              prize: [Number(match[5]), Number(match[6])]
            }
          })
        yield item[0]
      }
    }
  }

  play() {
    const result: any[] = []
    for (const { button, prize, seq } of this.arcade) {
      const memo: Record<string, string> = {}
      const { a: btnA, b: btnB } = button
      let position = [0, 0]
      const gap = [prize[0] / btnA[0], prize[1] / btnA[1],
      prize[0] / btnB[0], prize[1] / btnB[1]].filter(g => g > 141).map(Math.floor)

      if (gap.length > 3) {
        continue // impossible
      }

      explore(0, position)

      function explore(cost: number, [x, y]: number[], turnA = 0, turnB = 0) {
        if (memo[`${x}:${y}:${turnA}:${turnB}`]) return

        if (turnA + turnB > 200) return
        if (x > prize[0] || y > prize[1]) return
        memo[`${x}:${y}:${turnA}:${turnB}`] = "."

        if (x == prize[0] && y == prize[1]) {
          result.push(cost)
          console.log(seq, prize, turnA, turnB)
          return
        }
        // Navigate towards prize (cost no)
        const aDx = x + btnA[0], aDy = y + btnA[1], bDx = x + btnB[0], bDy = y + btnB[1]
        explore(cost + 3, [aDx, aDy], turnA + 1, turnB) // Press A for 3 tokens
        explore(cost + 1, [bDx, bDy], turnA, turnB + 1) // Press B for 1 token}}
      }
    }
    console.log(result.reduce((acc, cost) => acc + cost, 0)) // 29388

  }

}

// const q = new Question("./test2.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)
