// @jorjun 

import { cramerRule } from "../lib/matrix.ts"

interface Claw {
  seq: number
  button: { a: number[], b: number[] },
  prize: number[]
}

function* parse(section: string[]) {
  let seq = 0, error = 10000000000000
  for (const mac of section) {
    const item = Array.from(mac.matchAll(/Button A: X\+(\d{1,2}), Y\+(\d{1,2})\sButton B: X\+(\d{1,2}), Y\+(\d{1,2})\sPrize: X=(\d{1,5}), Y=(\d{1,5})/gm))
      .map((match) => {
        return {
          seq: seq++,
          button: { a: [Number(match[1]), Number(match[2])], b: [Number(match[3]), Number(match[4])] },
          prize: [error + Number(match[5]), error + Number(match[6])]
        }
      })
    yield item[0]
  }
}


class Question {
  arcade: Claw[] = []
  total = 0

  constructor (private path: string) { }

  async load() {
    const _data = await Deno.readTextFile(this.path)
    const section = _data.split("\n\n")
    this.arcade = Array.from(parse(section))

    for (const claw of this.arcade) {
      const { seq, button, prize } = claw
      const [x, y] = cramerRule([button.a, button.b], prize)
      if (x < 0 || y < 0) continue
      if (Math.floor(x) !== x || Math.floor(y) !== y) continue
      const px = x * button.a[0] + y * button.b[0]
      const py = x * button.a[1] + y * button.b[1]
      // if (px !== prize[0] || py !== prize[1]) continue // Overflow errors !!!!!
      console.log({ seq, x, y, prize, cp: [px, py], button })
      this.total += x * 3 + y
    }

  }
}

// const q = new Question("./test1.txt")
const q = new Question("./data.txt")
await q.load()
console.log(q.total)
