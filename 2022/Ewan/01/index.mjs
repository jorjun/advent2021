import fs from "fs";
const input = fs.readFileSync("input.txt", "utf8");

let currentRaindeer = [0, 0]; // [raindeer, calories]
let highestCalories = [0, 0]; // [raindeer, calories]

let currentValue = '';

console.log('input', input.length);

// loop each character in string
for (let i = 0; i < input.length; i++) {
    const char = input[i];

    const nextRaindeer = () => {
        // const calories = parseInt(currentValue);
        // currentRaindeer = [currentRaindeer[0] + 1, currentRaindeer[1] + calories];
        if(currentRaindeer[1] > highestCalories[1]) {
            console.log('new highest', currentRaindeer, i);
            highestCalories = [currentRaindeer[0], currentRaindeer[1]];
        }
        currentRaindeer[0]++;
        currentRaindeer[1] = 0;
        currentValue = '';
        i++;
    }

    if(char === "\n" && input[i+1] !== "\n") {
        currentRaindeer[1] += parseInt(currentValue);
        currentValue = '';
        continue;
    }

    if(char === "\n" && input[i+1]=='\n') { // empty line
        nextRaindeer();
        continue;
    }


    currentValue+=char;

}

console.log('result', highestCalories);