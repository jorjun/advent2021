let data = Deno.readTextFileSync('./input.txt');
// console.log(data);

function part1() {
    let choices = {
        A: { // rock
            win: "C",
            value: 1
        },
        B: { // paper
            win: "A",
            value: 2
        },
        C: { // scissors
            win: 'B',
            value: 3
        }
    }
    
    let winValue = 6;
    let drawValue = 3;
    let loseValue = 0;
    
    let totalPoints = 0;
    data = data.replace(/X/g, "A");
    data = data.replace(/Y/g, "B");
    data = data.replace(/Z/g, "C");
    
    const rounds = data.split('\n');
    rounds.forEach(r => {
        let [p1, p2] = r.split(' ');
        let addedValue = choices[p2].value;
        if (p1 == p2) addedValue += drawValue;
        else if(p1 == choices[p2].win) addedValue += winValue;
        else addedValue += loseValue;        
        totalPoints += addedValue;
    })
    return totalPoints;
}


function part2() {
    let choices = {
        A: { // rock
            win: "C",
            lose: "B",
            value: 1
        },
        B: { // paper
            win: "A",
            lose: "C",
            value: 2
        },
        C: { // scissors
            win: 'B',
            lose: 'A',
            value: 3,
        }
    }

    let winValue = 6;
    let drawValue = 3;
    let loseValue = 0;

    let options = ['A', 'B', 'C'];
    let outcomes = ['L', 'D', 'W'];

    data = data.replace(/X/g, "A");
    data = data.replace(/Y/g, "B");
    data = data.replace(/Z/g, "C");

    const rounds = data.split('\n');
    let totalPoints = 0;
    rounds.forEach(r => {
        let [p1, p2] = r.split(' ');
        const targetOutcome = outcomes[options.indexOf(p2)];
        let addedValue = 0;
        if(targetOutcome == 'W') addedValue = choices[choices[p1].lose].value + winValue;
        else if(targetOutcome == 'D') addedValue = choices[p1].value + drawValue;
        else if(targetOutcome == 'L') addedValue = choices[choices[p1].win].value + loseValue;        
        totalPoints += addedValue;
    });
    return totalPoints;
}

console.time("part2")
console.log('part1:', part1());
console.log('part2:', part2());
console.timeEnd("part2")
