// Vviii ☉ : ♐ ☽ : ♍

enum Side {
  left,
  right,
}

interface Data {
  [Side.left]: string[];
  [Side.right]: string[];
}

export class Advent {
  data: Data = {
    [Side.left]: [],
    [Side.right]: [],
  };

  load = async (path: string) => {
    const raw = (await Deno.readTextFile(path)).split("\n").filter((_) =>
      _ !== ""
    );
    for (let ix = 0; ix < raw.length; ix++) {
      const side = ix % 2 as Side;
      this.data[side].push(raw[ix]);
    }
  };

  output() {
    for (let ix = 0; ix < this.data[Side.left].length; ix++) {
      const [left, right] = [
        this.data[Side.left][ix],
        this.data[Side.right][ix],
      ];
      console.log({ left, right });
    }
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/sample.dat");
  console.log(q.output());
}
