// @jorjun  Vviii ☉ : ♐ ☽ : ♉
// --- Day 7: No Space Left On Device ---

class Advent {
  input: string[] = [];

  async load(path: string) {
    this.input = (await Deno.readTextFile(path)).split("\n");
  }
  output() {
    let path: string[] = [];
    let size: Record<string, number> = {};
    for (const row of this.input) {
      const bits = row.split(" ");
      if (bits[0] == "$") {
        if (bits[1] == "cd") {
          if (bits[2] == "..") {
            path.pop();
          } else {
            const _path = bits[2] != "/" ? `${bits[2]}/` : "/";
            path.push(_path);
          }
        }
      } else if (bits[0].match(/\d+/)) {
        const file_size = Number(bits[0]);
        for (let ix = 0; ix < path.length; ix++) {
          const _path = path.slice(0, ix + 1).join("");
          if (size[_path] == undefined) size[_path] = 0;
          size[_path] += file_size;
        }
      }
    }
    console.log(size);
    const free = 70000000 - size["/"];
    console.log({ free });
    return Object.values(size).filter((_) => (free + _) >= 30000000)
      .sort()[0];
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/data.txt");
  console.log(q.output());
}
