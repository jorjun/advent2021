// Q1 - Vviii ☉ : ♐ ☽ : ♓

class Question {
  calory_list = [0];
  constructor(private path = "./d1.txt") {}

  async load(ix=0) {
    const data = await Deno.readTextFile(this.path)!;
    for (const _line of data.split("\n")) {
      if (_line == "") {
        this.calory_list[ix++] = 0;
      } else {
        this.calory_list[ix] += Number(_line);
      }
    }
  }
  output = () => this.calory_list.sort((a, b) => b - a).slice(0, 3).reduce((prv, cur) => prv + cur)
}
// main
const q = new Question();
await q.load();
console.log(q.output());
