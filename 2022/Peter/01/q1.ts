// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♓

class Question {
  calory_list: number[] = [];
  constructor(private path = "./d1.txt") {}

  async load() {
    const data = await Deno.readTextFile(this.path).catch((err) =>
      console.error(err)
    );
    if (!data) throw `unable to read: ${this.path}`;

    let elf = 0;
    for (const _line of data.split("\n")) {
      if (_line == "") {
        elf++;
        this.calory_list.push(0);
      } else {
        const sweet = Number(_line);
        this.calory_list[elf] += sweet;
      }
    }
  }
  output() {
    let biggest = { ix: 0, cal: 0 }, ix = 0;
    for (const cal of this.calory_list) {
      if (cal > biggest.cal) {
        biggest = { ix, cal };
      }
      ix++;
    }
    return biggest;
  }
}

const q = new Question();
await q.load();
console.log(q.output())
