// Vviii ☉ : ♐ ☽ : ♍

interface Packet {
  packet: Packet | number[];
}

export class Advent {
  async *load(path: string) {
    const data = await Deno.readTextFile(path);
    for (const line of data.split("\n\n")) {
      const [left, right] = line.split("\n");
      yield { left, right };
    }
  }

  compare(left: any, right: any) {
    if (Array.isArray(left))
      for (const item of left) {
        
      }
  }

  async output(path: string) {
    for await (const { left, right } of this.load(path)) {
      const [lp, rp] = [left, right].map((_) => JSON.parse(_));
      for (let ix = 0; ix < lp.length; ix++) {
        if (lp) {
          if (lp[ix] < rp[ix]) return ix + 1;
        }
      }
    }
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  q.output("data/sample.dat");
}
