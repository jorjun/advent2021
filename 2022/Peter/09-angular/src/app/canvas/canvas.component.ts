import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';

export interface ComponentInput {
  height: number;
  width: number;
  back_color: string;
}

@Component({
  selector: 'app-canvas',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.sass'],
})
export class CanvasComponent implements AfterViewInit {
  @Input() attr!: ComponentInput;
  @ViewChild('canvas') canvasRef!: ElementRef<HTMLCanvasElement>;

  ctx!: CanvasRenderingContext2D;

  clear() {
    console.log('clear');
    const ctx = this.ctx,
      { width, height, back_color } = this.attr;
    ctx.fillStyle = back_color;
    ctx.fillRect(0, 0, width, height);
    ctx.fill();
  }

  ngAfterViewInit() {
    const canvas = this.canvasRef.nativeElement;
    const ctx = canvas.getContext('2d');
    if (!ctx) throw 'unable to get render context';
    this.ctx = ctx;
  }
}
