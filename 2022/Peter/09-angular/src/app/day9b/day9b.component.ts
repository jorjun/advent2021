// Vviii ☉ : ♐ ☽ : ♋

type Knot = { x: number; y: number; char: string };
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import {
  CanvasComponent,
  ComponentInput as CanvasAttr,
} from '../canvas/canvas.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import {
  animationFrameScheduler,
  interval,
  Subject,
  Subscription,
  takeUntil,
} from 'rxjs';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-day9a',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    CanvasComponent,
    HttpClientModule,
    MatButtonModule,
    MatListModule,
    RouterModule,
  ],
  templateUrl: './day9b.component.html',
  styleUrls: ['./day9b.component.sass'],
})
export class Day9bComponent implements AfterViewInit {
  @ViewChild('canvas', { static: true }) canvas!: CanvasComponent;

  @Input() data_url = '/assets/data/data.dat';

  input: string[] = [];
  input_page: string[] = [];
  visited: Record<string, boolean> = {};
  tail_count = 0;
  CMD: Record<string, number[]> = {
    U: [0, -1],
    D: [0, 1],
    L: [-1, 0],
    R: [1, 0],
  };
  font = '16px Courier'; // height
  font_char!: TextMetrics;
  page = 0;
  page_len = 4;
  page_top = 0;
  step = 0;
  cols = 300;
  scale = 1;

  attr: CanvasAttr = { width: 800, height: 600, back_color: 'black' };
  animationFrame$ = interval(0, animationFrameScheduler);
  animStop?: Subject<void>;
  obsFrame?: Subscription;

  start: Knot = { x: 0, y: 0, char: 'S' };
  head: Knot = { x: 0, y: 0, char: 'H' };
  tail: Knot[] = [];

  constructor(private http: HttpClient, private cdr: ChangeDetectorRef) {
    this.scale = Math.floor(this.attr.width / this.cols);
  }

  step_click = () => {
    if (this.step < this.input.length) this.animFrame();
    this.page = Math.floor(this.step / this.page_len);
    this.page_top = this.page * this.page_len;
    this.input_page = this.input.slice(
      this.page_top,
      this.page_top + this.page_len
    );
    this.cdr.detectChanges();
  };

  *moveSteps(delta: { x: number; y: number }) {
    for (let _ = 0; _ < Math.abs(delta.x + delta.y); _++) {
      this.head.x += Math.sign(delta.x); // Grr
      this.head.y += Math.sign(delta.y);
      yield null;
    }
  }

  plot({ x, y, char }: Knot) {
    const ctx = this.canvas.ctx,
      { scale } = this;
    ctx.font = this.font;
    ctx.fillStyle = 'red';

    ctx.beginPath();
    ctx.fillText(char, x * scale, y * scale);
    ctx.stroke();
  }

  animFrame() {
    this.canvas.clear();
    // this.grid_lines();

    const { tail: ten_tails, start, CMD } = this,
      [cmd, _move] = this.input[this.step].split(' '),
      move = Number(_move),
      [x, y] = CMD[cmd].map((_) => _ * move);
    // ---------------------------
    let head: Knot;
    for (const _ of this.moveSteps({ x, y })) {
      for (let ix = 0; ix < this.tail.length - 1; ix++) {
        const tail = ten_tails[ix];
        if (ix == 0) head = this.head;
        else head = ten_tails[ix - 1];
        const dx = head.x - tail.x,
          dy = head.y - tail.y;
        if (Math.abs(dx) > 1 || Math.abs(dy) > 1) {
          tail.x += Math.sign(dx);
          tail.y += Math.sign(dy);
        }
        if (tail.char == '9') this.visited[`${tail.x}-${tail.y}`] = true;
      }
    }
    for (const tail of this.tail) this.plot(tail);
    // ---------------------------
    this.plot(start);
    this.plot(this.head);
    this.step += 1;
    this.tail_count = Object.keys(this.visited).length;
    if (this.step >= this.input.length) this.pause();
  }

  grid_lines() {
    const { ctx } = this.canvas,
      { width, height } = this.attr;
    ctx.lineWidth = 0.5;
    const dx = Math.floor(width / this.cols),
      dy = Math.floor(height / this.cols);
    ctx.strokeStyle = 'white';
    for (let y = 0; y < height; y += dy) {
      ctx.moveTo(0, y); // | =>
      ctx.lineTo(width, y); // Horiz
      for (let x = 0; x < width; x += dx) {
        ctx.moveTo(x, 0); // | vertical
        ctx.lineTo(x, height);
      }
    }
    ctx.stroke();
  }

  play = () => {
    this.animStop = new Subject<void>();
    this.obsFrame = this.animationFrame$
      .pipe(takeUntil(this.animStop))
      .subscribe((_) => this.animFrame());
  };

  pause() {
    this.animStop?.next();
    this.animStop?.complete();
    this, this.cdr.detectChanges();
  }

  reset() {
    const { ctx } = this.canvas;
    this.pause();
    this.canvas.clear();
    // this.grid_lines();
    this.step = 0;
    this.start = { x: 10, y: 250, char: ' ' };
    this.tail = Array.from({ length: 100 }, (_, ix) => ix).map((_) => ({
      x: this.start.x,
      y: this.start.y,
      char: `.`,
    }));
    this.head = { ...this.start, char: 'H' };
    this.plot(this.start);
    this.visited = {};
    this.cdr.detectChanges();
  }

  ngAfterViewInit(): void {
    this.font_char = this.canvas.ctx.measureText('H');

    this.http
      .get(this.data_url, {
        responseType: 'text',
      })
      .subscribe((input) => {
        this.input = input.split('\n');
        this.reset();
      });
  }

  ngOnDestroy = () => {
    this.pause();
    this.obsFrame?.unsubscribe();
  };
}
