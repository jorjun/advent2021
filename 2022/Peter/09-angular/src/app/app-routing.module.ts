import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Day9aComponent } from './day9a/day9a.component';
import { Day9bComponent } from './day9b/day9b.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'day9a', component: Day9aComponent },
  { path: 'day9b', component: Day9bComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
