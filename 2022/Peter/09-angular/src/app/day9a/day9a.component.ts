// Vviii ☉ : ♐ ☽ : ♋

type Coord = { x: number; y: number };
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import {
  CanvasComponent,
  ComponentInput as CanvasAttr,
} from '../canvas/canvas.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import {
  animationFrameScheduler,
  interval,
  Subject,
  Subscription,
  takeUntil,
} from 'rxjs';

@Component({
  selector: 'app-day9a',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    CommonModule,
    CanvasComponent,
    HttpClientModule,
    MatButtonModule,
    MatListModule,
  ],
  templateUrl: './day9a.component.html',
  styleUrls: ['./day9a.component.sass'],
})
export class Day9aComponent implements AfterViewInit {
  @ViewChild('canvas', { static: true }) canvas!: CanvasComponent;

  @Input() data_url = '/assets/data/data.dat';

  input: string[] = [];
  input_page: string[] = [];
  visited: Record<string, boolean> = {};
  tail_count = 0;
  CMD: Record<string, number[]> = {
    U: [0, -1],
    D: [0, 1],
    L: [-1, 0],
    R: [1, 0],
  };
  font = '22px Roboto'; // height
  font_char!: TextMetrics;
  page = 0;
  page_len = 10;
  page_top = 0;
  step = 0;
  cols = 250;
  scale = 1;

  attr: CanvasAttr = { width: 800, height: 600, back_color: 'black' };
  animationFrame$ = interval(0, animationFrameScheduler);
  animStop?: Subject<void>;
  obsFrame?: Subscription;

  start: Coord = { x: 0, y: 0 };
  head: Coord = { x: 0, y: 0 };
  tail: Coord = { x: 0, y: 0 };

  constructor(private http: HttpClient, private cdr: ChangeDetectorRef) {
    this.scale = Math.floor(this.attr.width / this.cols);
  }

  step_click = () => {
    if (this.step < this.input.length) this.animFrame();
    this.page = Math.floor(this.step / this.page_len);
    this.page_top = this.page * this.page_len;
    this.input_page = this.input.slice(
      this.page_top,
      this.page_top + this.page_len
    );
    this.cdr.detectChanges();
  };

  *moveSteps(delta: Coord) {
    for (let _ = 0; _ < Math.abs(delta.x + delta.y); _++) {
      this.head.x += Math.sign(delta.x); // Grr
      this.head.y += Math.sign(delta.y);
      yield null;
    }
  }

  plot({ x, y }: Coord, char: string) {
    const ctx = this.canvas.ctx,
      { scale } = this;
    ctx.font = this.font;
    ctx.fillStyle = 'red';

    ctx.beginPath();
    ctx.fillText(
      char,
      x * scale + this.scale / 2 - this.font_char.width / 2,
      y * scale
    );
    ctx.stroke();
  }

  animFrame() {
    this.canvas.clear();
    // this.grid_lines();

    const { head, tail, start, CMD } = this,
      [cmd, _move] = this.input[this.step].split(' '),
      move = Number(_move),
      [x, y] = CMD[cmd].map((_) => _ * move);
    // ---------------------------
    for (const _ of this.moveSteps({ x, y })) {
      const dx = head.x - tail.x,
        dy = head.y - tail.y;
      if (Math.abs(dx) > 1 || Math.abs(dy) > 1) {
        tail.x += Math.sign(dx);
        tail.y += Math.sign(dy);
      }
      this.visited[`${tail.x}-${this.tail.y}`] = true;
      if (head.x != tail.x || head.y != tail.y) this.plot(tail, 'T');
    }
    // ---------------------------
    this.plot(start, 'S');
    this.plot(head, 'H');
    this.step += 1;
    this.tail_count = Object.keys(this.visited).length;
    if (this.step >= this.input.length) this.pause();
  }

  grid_lines() {
    const { ctx } = this.canvas,
      { width, height } = this.attr;
    ctx.lineWidth = 0.5;
    const dx = Math.floor(width / this.cols),
      dy = Math.floor(height / this.cols);
    ctx.strokeStyle = 'white';
    for (let y = 0; y < height; y += dy) {
      ctx.moveTo(0, y); // | =>
      ctx.lineTo(width, y); // Horiz
      for (let x = 0; x < width; x += dx) {
        ctx.moveTo(x, 0); // | vertical
        ctx.lineTo(x, height);
      }
    }
    ctx.stroke();
  }

  play = () => {
    this.animStop = new Subject<void>();
    this.obsFrame = this.animationFrame$
      .pipe(takeUntil(this.animStop))
      .subscribe((_) => this.animFrame());
  };

  pause() {
    this.animStop?.next();
    this.animStop?.complete();
    this, this.cdr.detectChanges();
  }

  reset() {
    const { ctx } = this.canvas;
    this.pause();
    this.canvas.clear();
    this.grid_lines();
    this.step = 0;
    this.start = { x: 10, y: 90 };
    this.head = { ...this.start };
    this.tail = { ...this.head };
    this.plot(this.start, 'S');
    this.visited = {};
    this.cdr.detectChanges();
  }

  ngAfterViewInit(): void {
    this.font_char = this.canvas.ctx.measureText('H');

    this.http
      .get(this.data_url, {
        responseType: 'text',
      })
      .subscribe((input) => {
        this.input = input.split('\n');
        this.reset();
      });
  }

  ngOnDestroy = () => {
    this.pause();
    this.obsFrame?.unsubscribe();
  };
}
