type Instruct = { operand: string; val?: number };

export class Advent {
  input: string[] = [];
  cycle = 1;
  x = 1;
  obs = [20, 60, 100, 140, 180, 220];

  load = async (path: string) =>
    this.input = (await Deno.readTextFile(path)).split("\n");

  *op({ operand, val }: Instruct) {
    if (operand === "addx" && val) {
      yield ++this.cycle;
      this.x += val;
    }
    yield ++this.cycle; // Noop or addx
  }

  run() {
    let val = -1, sum = 0;
    for (const line of this.input) {
      const [operand, _val] = line.split(" ");
      if (_val) val = Number(_val);
      for (const cycle of this.op({ operand, val })) {
        if (this.obs.includes(cycle)) {
          const signal = cycle * this.x;
          console.log({ cycle, x: this.x, signal });
          sum += signal;
        }
      }
    }
    console.log(sum);
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/data.dat");
  q.run();
}
