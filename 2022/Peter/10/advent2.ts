type Instruct = { operand: string; val?: number };

export class Advent {
  input: string[] = [];
  cycle = 0;
  x = 1;

  load = async (path: string) =>
    this.input = (await Deno.readTextFile(path)).split("\n");

  *op({ operand, val }: Instruct) {
    yield ++this.cycle;
    if (operand === "addx") {
      yield ++this.cycle;
      this.x += val!;
    }
  }

  run() {
    let val = 0, row = Array<string>(40);
    for (const line of this.input) {
      const [operand, _val] = line.split(" ");
      if (_val) val = Number(_val);
      for (const cycle of this.op({ operand, val })) {
        const px = (cycle - 1) % 40;
        if (px == 0 && cycle > 1) console.log(row.join(""));
        row[px] = this.x >= px - 1 && this.x <= px + 1 ? "#" : ".";
      }
    }
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/data.dat");
  q.run();
}
