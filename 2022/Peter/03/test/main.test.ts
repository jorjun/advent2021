// @jorjun

import { assertEquals } from "https://deno.land/std@0.166.0/testing/asserts.ts";

import { Question as Q1} from "../q1.ts";
import { Question as Q2} from "../q2.ts";

// Question 1
Deno.test("Elf packing .I.", async (tst) => {
  const t1 = new Q1('data/test.txt')
  await t1.load()
  const t2 = new Q1('data/data.txt')
  await t2.load()

  await tst.step("test", async () => {
    const score = t1.output()
    assertEquals(score, 157);
  });

  await tst.step("real", async () => {
    const score = t2.output()
    assertEquals(score, 7701);
  });
});



// Question 2
Deno.test("Elf packing .II.", async (tst) => {
    const t1 = new Q2('data/test.txt')
  await t1.load()
    const t2 = new Q2('data/data.txt')
    await t2.load()
  
  await tst.step("Test", async () => {
    const score = t1.output()
    assertEquals(score, 70);
  });

  await tst.step("Real", async () => {
    const score = t2.output()
    assertEquals(score, 2644);
  });
});

