// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♈

type Group = { badge: string };

export class Question {
  model: Group[] = [];

  constructor(private path = "./d1.txt") {}

  priority(char: string) {
    const uni = char.charCodeAt(0),
      order = uni <= 90 ? uni - 65 + 26 : uni - 97;
    return order + 1;
  }

  common(group: string[]) {
    for (const char of group[0].split("")) {
      let found = 0;
      for (let iy = 1; iy < 3; iy++) {
        if (group[iy].includes(char)) {
          found++;
        }
      }
      if (found == 2) return char;
    }
    console.error("uh oh!")
    return "?";
  }

  async load() {
    await Deno.stat(this.path).catch(err => {throw new Deno.errors.NotFound(`${this.path}`)})
    const data = await Deno.readTextFile(this.path)!
    let group: string[] = [];
    for (
      const line of data
        .split("\n")
        .filter((_) => _ != "")
    ) {
      group.push(line);
      if (group.length === 3) {
        let badge = this.common(group);
        this.model.push({ badge });
        group = [];
      }
    }
  }

  output() {
    return this.model
      .map(_ => this.priority(_.badge))
      .reduce((prv, cur) => prv + cur)
  }
}

// Main only
if (import.meta.main) {
  const q = new Question("data/data.txt");
  await q.load();
  console.log(
    q.output()
  )
}
