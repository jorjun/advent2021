// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♈

type Compartment = { error: string };

export class Question {
  model: Compartment[] = [];

  constructor(private path = "data/data.txt") {}

  priority(char: string) {
    const uni = char.charCodeAt(0),
      order = uni <= 90 ? uni - 65 + 26 : uni - 97;
    return order + 1;
  }

  async load() {
    await Deno.stat(this.path).catch((err) => {
      throw new Deno.errors.NotFound(`${this.path}`);
    });
    const data = await Deno.readTextFile(this.path)!;

    this.model = data.split("\n").filter((_) => _ != "").map((line) => {
      const half = line.length / 2,
        [left, right] = [
          line.slice(0, half),
          line.slice(half, line.length),
        ],
        error = left.split("").filter((_) => right.includes(_))[0];
      return { error };
    });
  }

  output() {
    return this.model
      .map((_) => this.priority(_.error))
      .reduce((prv, cur) => cur + prv);
  }
}

// Main only
if (import.meta.main) {
  const q = new Question();
  await q.load();
  console.log(q.output());
}
