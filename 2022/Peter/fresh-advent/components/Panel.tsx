//  Vviii ☉ : ♐ ☽ : ♋

interface Props {
  start: number;
  width: number;
  height: number;
  content: string;
}

export default function Canvas(
  { width, height, ...props }: Props,
) {
  return (
    <div class="flex flex-col gap-2 w-full" style={{ width: width }}>
      <textarea
        className="border-2 border-gray-200"
        width={width}
        height={height}
      />
    </div>
  );
}
