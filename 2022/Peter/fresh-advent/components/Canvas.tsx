//  Vviii ☉ : ♐ ☽ : ♋

import { MutableRef, useState } from "preact/hooks";
// import { signal } from "preact/signals";

interface Props {
  start: number;
  width: number;
  height: number;
  step: number;
  canvasRef: MutableRef<HTMLCanvasElement | null>;
  onLoad: () => void;
}

export default function Canvas(
  { width, height, step, canvasRef, onLoad, ...props }: Props,
) {
  return (
    <div class="flex flex-col gap-2 w-full" style={{ width: width }}>
      <a href="/">HOME</a>
      <canvas
        className="border-2 border-gray-200"
        width={width}
        height={height}
        ref={canvasRef}
        onLoad={onLoad}
      />
    </div>
  );
}
