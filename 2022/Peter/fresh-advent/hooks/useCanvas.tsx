//  Vviii ☉ : ♐ ☽ : ♋

// import { Attributes, Component, ComponentChild, ComponentChildren, Ref } from "preact";
import { h } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";

export default function useCanvas() {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const [canvas, setCanvas] = useState<HTMLCanvasElement | null>(null);
  const [ctx, setContext] = useState<CanvasRenderingContext2D | null>(null);

  useEffect(() => {
    if (canvasRef.current) {
      setCanvas(canvasRef.current);
      setContext(canvasRef.current.getContext("2d"));
    }
  }, [canvasRef]);

  return { canvas, ctx, setCanvas, canvasRef };
}
