//  Vviii ☉ : ♐ ☽ : ♋

import { useState } from "preact/hooks";
import { Button } from "../components/Button.tsx";
import Canvas from "../components/Canvas.tsx";

import useCanvas from "@/hooks/useCanvas.tsx";
import { Grid } from "@/lib/gfx.ts";

export default function Part1(
  { size }: { size: { width: number; height: number } },
) {
  const { canvas, setCanvas, ctx, canvasRef } = useCanvas();
  const [step, setSteps] = useState(0);

  const onLoad = (): void => {
    console.log("loaded");
    if (!ctx) return;
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, size.width, size.height);
    const grid = new Grid(ctx, 500, 500, 50, 50);
    grid.grid_lines();
  };

  function process_step(ctx: CanvasRenderingContext2D, step: number) {
    ctx.strokeStyle = "red";
    ctx.lineWidth = 2;
    const x = Math.random() * 500, y = Math.random() * 500;
    ctx.lineTo(x, y);
    ctx.stroke();
  }

  function handleStep() {
    if (!ctx) return;
    process_step(ctx, step);
    setSteps(step + 1);
  }

  return (
    <div className="flex justify-center pt-10 items-center flex-col">
      <img
        src="/day-9.jpg"
        class="w-32 h-32"
        alt="elf looking out of his tree house"
      />
      <Canvas
        width={size.width}
        height={size.height}
        step={step}
        canvasRef={canvasRef}
        start={1}
        onLoad={onLoad}
      />
      <p class="flex-grow-1 text-xl">Step: {step}</p>
      <Button onClick={handleStep}>Next</Button>
    </div>
  );
}
