//  Vviii ☉ : ♐ ☽ : ♋

export class Grid {
  constructor(
    private ctx: CanvasRenderingContext2D,
    private width: number,
    private height: number,
    private nCol: number,
    private nRow: number,
  ) {}

  grid_lines() {
    const dx = Math.floor(this.width / this.nCol),
      dy = Math.floor(this.height / this.nRow);
    this.ctx.strokeStyle = "white";
    for (let y = 0; y < this.height; y += dy) {
      this.ctx.moveTo(0, y); // | =>
      this.ctx.lineTo(this.width, y); // Horiz
      for (let x = 0; x < this.width; x += dx) {
        this.ctx.moveTo(x, 0); // | vertical
        this.ctx.lineTo(x, this.height);
      }
    }
    this.ctx.stroke();
  }
}
