import { PageProps } from "$fresh/server.ts";

import Part1 from "@/islands/Part1.tsx";

export default function _(props: PageProps) {
  return (
    <Part1
      size={{
        width: 500,
        height: 500,
      }}
    />
  );
}
