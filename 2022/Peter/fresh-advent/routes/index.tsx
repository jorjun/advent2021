//  Vviii ☉ : ♐ ☽ : ♋

import { Head } from "$fresh/runtime.ts";

export default function _() {
  return (
    <>
      <Head>
        <title>Fresh App</title>
      </Head>
      <div class="p-4 mx-auto max-w-screen-md">
        <a href="/day/9/part1">
          <img
            src="/day-9.jpg"
            class="w-32 h-32"
            alt="elf looking out of his tree house"
          />
          Day-9 .I.
        </a>
        <a href="/day/9/part2">
          <img
            src="/day-9.jpg"
            class="w-32 h-32"
            alt="elf looking out of his tree house"
          />
          Day-9 .II.
        </a>
        <p class="my-6">
          Advent of Code 2022.
        </p>
      </div>
    </>
  );
}
