// @jorjun

import { assertEquals } from "https://deno.land/std@0.166.0/testing/asserts.ts";

import { Question } from "./q1.ts";

Deno.test("Rock Paper Scissors", async (tst) => {
  const q = new Question('./q1.test.txt')
  await q.load()

  await tst.step("score", async () => {
    const score = q.output()
    assertEquals(score, 15);
  });
});
