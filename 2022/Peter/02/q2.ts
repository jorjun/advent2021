// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♈

// Rock, paper, scissors, me: them
const hand_code: Record<string, string> = {
  "A": "R",
  "B": "P",
  "C": "S",
  "X": "L",
  "Y": "D",
  "Z": "W",
};

// Rock, paper, scissors, them: me
const hand_win: Record<string, string> = {
  "R": "P",
  "P": "S",
  "S": "R",
};

const hand_lose: Record<string, string> = {
  "R": "S",
  "S": "P",
  "P": "R"
}

// Rock, paper, scissors
const hand_value: Record<string, number> = {
  "R": 1,
  "P": 2,
  "S": 3,
};

export class Question {
  model: Record<string, string>[] = [{}];
  score = 0;

  constructor(private path = "./d1.txt") {}

  async load() {
    const data = await Deno.readTextFile(this.path)
    if (!data) throw `unable to read: ${this.path}`;

    let ix = 0, me;
    for (const _line of data.split("\n")) {
      const [them, tactic] = _line.split(" ").map((_) => hand_code[_]);
      if (tactic === "W") me = hand_win[them];
      else if (tactic === "D") me = them;
      else me = hand_lose[them];
      this.model[ix++] = { me, them };
    }
  }

  output(): number {
    let score = 0;
    for (const { them, me } of this.model) {
      score += hand_value[me];
      if (me === them) score += 3
      else if (hand_win[them] === me) score += 6
    }
    return score;
  }
}

const q = new Question();
await q.load();
console.log(q.output());
