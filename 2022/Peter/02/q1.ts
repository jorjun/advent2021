// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♈

// Rock, paper, scissors, me: them
const hand_code: Record<string, string> = {
  "A": "R",
  "B": "P",
  "C": "S",
  "X": "R",
  "Y": "P",
  "Z": "S",
};

// Rock, paper, scissors, them: me
const hand_win: Record<string, string> = {
  "R": "P",
  "P": "S",
  "S": "R",
};

// Rock, paper, scissors
const hand_value: Record<string, number> = {
  "R": 1,
  "P": 2,
  "S": 3,
};

// Win, draw, lose
const win_value: Record<string, number> = {
  "W": 6,
  "D": 3,
  "L": 0,
};

export class Question {
  model: Record<string, string>[] = [{}];

  constructor(private path = "./d1.txt") {}

  async load() {
    const data = await Deno.readTextFile(this.path)
    if (!data) throw `unable to read: ${this.path}`;

    this.model = data.split("\n").map((line) => {
       const [them, me] = line.split(' ').map(_=>hand_code[_])
      return {me, them}
    })

  }

  output(): number {
    let score = 0;
    for (const { them, me } of this.model) {
      score += hand_value[me];
      let result = 'L'
      if (me === them) result = "D";
      else if (hand_win[them] === me) result = "W";
      score += win_value[result]
    }
    return score;
  }
}

const q = new Question('./q1.test.txt');
await q.load();
console.log(q.output());
