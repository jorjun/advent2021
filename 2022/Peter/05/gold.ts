// @jorjun Vviii ☉ : ♐ ☽ : ♉

import { readLines } from "@deno/std/io/buffer.ts";

// * Input: stacks of crates, n columns, 0 idx at bottom
//         [R]
//     [D] [G] [W]
// [B] [W] [F] [L]
//  1   2   3   4
//
// * Orders (after blank line):
// move 3 from 1 to 3

type Stack = { [col: number]: String[] };

export class Advent {
  stack: Stack = {};

  crate_lines: string[] = []
  column_line: string = "";
  order_lines: string[] = [];

  async load(path = "data/sample.txt") {
    const input = await Deno.readTextFile(path);
    this.stack = [];
    this.crate_lines = [...input.match(/[ ]*\[.*\]/gm) ?? []]
    this.column_line = [...input.match(/ \d{1}\s\s/g) ?? []].join("")
    this.order_lines = [...input.match(/move.* \d{1,2}/gm) ?? []]

    for (let ix = this.crate_lines.length - 1; ix >= 0; ix -= 1) {
      const line = this.crate_lines[ix]
      for (let col = 1; col <= 9; col++) {
          const pos = this.column_line.indexOf(String(col));
        if (line[pos] && line[pos].match(/[A-Z]/g)) {
          if (this.stack[col] == undefined) this.stack[col] = [];
          this.stack[col].push(line[pos]);
        }
      }
    }

    for (const order of this.order_lines) {
      const [count, from, to] = Array.from(
        order.matchAll(/(\d{1,2})/g) || [],
      ).map((_) => Number(_[1]));
      const fetch = this.stack[from].splice(-1 * count)
      this.stack[to].push(...fetch);
    }
  }

  output() {
    return Object.keys(this.stack)
      .map((_) => this.stack[Number(_)].slice(-1)[0])
      .join("");
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load();
  console.log(q.output());
}
