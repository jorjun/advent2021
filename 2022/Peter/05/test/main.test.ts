// @jorjun  Vviii ☉ : ♐ ☽ : ♈

import { assertEquals } from "@deno/std/testing/asserts.ts";

import { Advent as Silver } from "../silver.ts";
import { Advent as Gold } from "../gold.ts";

Deno.test("Silver. Day 5: Supply Stacks", async (tst) => {
  const silver = new Silver()
  await tst.step("Sample", async () => {
    await silver.load("data/sample.txt");
    assertEquals(silver.output(), "CMZ");
  });

  await tst.step("Real", async () => {
  const silver = new Silver()
    await silver.load("data/data.txt");
    assertEquals(silver.output(), 'FJSRQCFTN');
  });
});

Deno.test("Gold. Day 5: Supply Stacks", async (tst) => {
  const gold = new Gold()
  await tst.step("Sample", async () => {
    await gold.load("data/sample.txt");
    assertEquals(gold.output(), "MCD");
  });

  await tst.step("Real", async () => {
    const gold = new Gold()
    await gold.load("data/data.txt");
    assertEquals(gold.output(), "CJVLJQPHS");
  });
});
