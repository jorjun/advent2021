// @jorjun Vviii ☉ : ♐ ☽ : ♉

import { readLines } from "@deno/std/io/buffer.ts";

// * Input: stacks of crates, n columns, 0 idx at bottom
//         [R]
//     [D] [G] [W]
// [B] [W] [F] [L]
//  1   2   3   4
//
// * Orders (after blank line):
// move 3 from 1 to 3

export class Advent {
  stack: String[][] = [[]];

  async load(path = "data/sample.txt") {
    const input = await Deno.readTextFile(path),
      crate_lines = [...input.match(/[ ]*\[.*\]/gm) ?? []];
    const crate_map = crate_lines.map((_, row) => ({
      d: _,
      row: crate_lines.length - row,
    }));
    const column_line = [...input.match(/ \d{1}\s\s/g) ?? []].join("");

    const grid = Array(9).fill(null).map((_) => Array(9).fill(""));

    crate_map.forEach((line, y) =>
      [...column_line.match(/\d{1}/g) ?? []]
        .forEach((col) => {
          const ch = line.d[column_line.indexOf(col)];
          if (ch) {
            console.log({ col, ch, y });
            grid[y][Number(col) - 1] = `${ch}`;
          }
        })
    );

    for (const order of [...input.match(/move.* \d{1,2}/gm) ?? []]) {
      const [count, from, to] = Array.from(
        order.matchAll(/(\d{1,2})/g) || [],
      ).map((_) => Number(_[1]));
      const fetch = this.stack[from].splice(-1 * count).reverse();
      this.stack[to].push(...fetch);
    }
  }

  output = () => this.stack.map((_) => _.slice(-1)[0]).join("");
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load();
  console.log(q.output());
}
