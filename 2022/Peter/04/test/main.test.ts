// @jorjun  Vviii ☉ : ♐ ☽ : ♈

import { assertEquals } from "@deno/std/testing/asserts.ts";

import { Advent as Silver } from "../silver.ts";
import { Advent as Gold } from "../gold.ts";

Deno.test("Silver: Elf Camp Cleanup", async (tst) => {
  const silver = new Silver()
  await tst.step("Sample", async () => {
    await silver.load("data/sample.txt");
    assertEquals(silver.output(), 2);
  });

  await tst.step("Real", async () => {
    await silver.load("data/data.txt");
    assertEquals(silver.output(), 490);
  });
});

Deno.test("Gold: Elf Camp Cleanup", async (tst) => {
  const gold = new Gold()
  await tst.step("Sample", async () => {
    await gold.load("data/sample.txt");
    assertEquals(gold.output(), 4);
  });

  await tst.step("Real", async () => {
    await gold.load("data/data.txt");
    assertEquals(gold.output(), 921);
  });
});
