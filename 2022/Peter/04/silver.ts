// @jorjun advent of code 2022
// Q1 - Vviii ☉ : ♐ ☽ : ♈

type Pair = {left: number, right: number}
type Assignment = { top: Pair; bottom: Pair };

export class Advent {
  model: Assignment[] = [];

  is_contained({ top, bottom }: Assignment) {
    let top_is_contained = (top.left >= bottom.left && top.right <= bottom.right),
      bottom_is_contained = (bottom.left >= top.left && bottom.right <= top.right)
    return top_is_contained || bottom_is_contained
  }

  async load(path = "data/data.txt") {
    const data = await Deno.readTextFile(path)!;
    this.model = data.split("\n").map((line) => {
      const [top, bottom] =
        line.split(",")
          .map((_) => _.split("-"))
          .map(([left, right]) => ({ left: Number(left), right: Number(right) }))
      return { top, bottom };
    });
  }

  output() {
    return this.model.map((_) => this.is_contained(_))
      .filter((_) => _)
      .length;
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load();
  console.log(q.output());
}
