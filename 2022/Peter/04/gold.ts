// --- Day 4: Camp Cleanup ---
// GOLD - Vviii ☉ : ♐ ☽ : ♈

import { readLines} from '@deno/std/io/buffer.ts'

// "2-4" => {left:2, right: 4}
type Elf_Pair_Range = { left: number, right: number } 

// "2-4,6-8" => {top: {left:2, right: 4}, bottom: {left:6, right: 8}}
type Assignment = { top: Elf_Pair_Range; bottom: Elf_Pair_Range };

export class Advent {

  model: Assignment[] = []

  is_overlapping = ({ top, bottom }: Assignment) => !(top.right < bottom.left || top.left > bottom.right)

  async load(path = "data/data.txt"): Promise<void> {
    const input = await Deno.open(path, {read: true});
    this.model = [] // N.B. Needed for unit testing conceits to pass
    for await (const line of readLines(input)) {
      const elf_pair = line.split(","),         // ["2-4", "6-8"]

        [top, bottom] = elf_pair.map((_) => _.split("-")) 
          .map(([a, b]) => ({ left: Number(a), right: Number(b) }))

      this.model.push( { top, bottom})
    }
    input.close()
  }
  
  output = () => this.model.map((_) => this.is_overlapping(_)).filter((_) => _).length;
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load();
  console.log(q.output());
}
