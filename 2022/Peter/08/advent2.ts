// @jorjun  Vviii ☉ : ♐ ☽ : ♊
// --- Day 8: Treetop Tree House-- -

type Coord = [x: number, y: number];

export class Advent {
  grid: number[][] = [[]];
  async load(path: string) {
    const raw = (await Deno.readTextFile(path)).split("\n");
    this.grid = raw.map((_) => _.split("").map(Number));
  }

  in_grid = (vert: number) => vert >= 0 && vert <= this.grid.length - 1;
  edge = (vert: number) => vert % (this.grid.length - 1) == 0;

  get_score([x, y]: Coord) {
    let tree_count = [0, 0, 0, 0]; // North South East West
    [[0, 1], [0, -1], [1, 0], [-1, 0]].forEach((inc, direction) => {
      let [vx, vy] = [x, y]; // reset grid viewpoint
      while (this.in_grid(vx + inc[0]) && this.in_grid(vy + inc[1])) {
        vx += inc[0];
        vy += inc[1];
        tree_count[direction]++;
        if (this.grid[vy][vx] >= this.grid[y][x]) break; // Taller tree
      }
    });
    return tree_count.filter((_) => _ > 0).reduce((a, b) => a * b);
  }

  output() {
    return this.grid.map((row, y) =>
      row.map((_, x) => {
        if (this.edge(x) || this.edge(y)) return 0; // Not clear in Question. Naughty
        return this.get_score([x, y]);
      }).reduce((acc, cur) => acc = Math.max(acc, cur))
    ).reduce((acc, cur) => acc = Math.max(acc, cur));
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/data.txt");
  console.log(q.output());
}
