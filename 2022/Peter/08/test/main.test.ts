// @jorjun  Vviii ☉ : ♐ ☽ : ♉

import { assertEquals } from "@deno/std/testing/asserts.ts";

import { Advent } from "../advent.ts";
import { Advent as Advent2 } from "../advent2.ts";

Deno.test("Q1. Day 8: Treetop house", async (tst) => {
  const advent = new Advent();
  await advent.load("data/sample.txt");
  console.log(advent.grid);

  await tst.step("Number Hidden", async () => {
    assertEquals(advent.isVisible([0, 0]), true);
    assertEquals(advent.grid.length, 5);
    assertEquals(advent.isVisible([4, 0]), true);
  });
  await tst.step("Top left", async () => {
    assertEquals(advent.isVisible([1, 1]), true);
  });
  await tst.step("Top Middle", async () => {
    assertEquals(advent.isVisible([2, 1]), true);
  });
  await tst.step("Top Right", async () => {
    assertEquals(advent.isVisible([3, 1]), false);
  });
  await tst.step("Centre 3", async () => {
    assertEquals(advent.isVisible([2, 2]), false);
  });
  await tst.step("Bottom 3 & 4", async () => {
    assertEquals(advent.isVisible([3, 1]), false);
    assertEquals(advent.isVisible([3, 3]), false);
  });
  await tst.step("3-2 - Visible", async () => {
    assertEquals(advent.isVisible([3, 2]), true);
  });
  await tst.step("3-2 - Visible", async () => {
    await advent.load("data/sample.txt");
    assertEquals(advent.output(), 21);
  });
});

Deno.test("Q2. Day 8: Treetop house", async (tst) => {
  const advent = new Advent2();
  await tst.step("Best tree", async () => {
    await advent.load("data/sample.txt");
    assertEquals(advent.output(), 8);
  });
  await tst.step("Best tree - real", async () => {
    await advent.load("data/data.txt");
    assertEquals(advent.output(), 374400);
  });
});
