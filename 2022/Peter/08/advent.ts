// @jorjun  Vviii ☉ : ♐ ☽ : ♊
// --- Day 8: Treetop Tree House-- -

type Coord = [x: number, y: number];

export class Advent {
  grid: number[][] = [[]];
  async load(path = "data/sample.txt") {
    const raw = (await Deno.readTextFile(path)).split("\n");
    this.grid = raw.map((_) => _.split("").map(Number));
  }
  edge = (vert: number) => vert % (this.grid.length - 1) === 0;

  sight_check([x, y]: Coord) {
    const hide_map: Record<string, boolean> = {}, height = this.grid[y][x];
    for (const dir of ["x", "y"]) {
      for (const inc of [-1, 1]) {
        let [sx, sy] = [x, y];
        while (true) {
          if (dir === "x") sx += inc;
          if (dir === "y") sy += inc;
          if (this.grid[sy][sx] >= height) {
            hide_map[`${dir}:${inc}`] = true;
            break;
          }
          if (dir === "x" && this.edge(sx)) break;
          if (dir === "y" && this.edge(sy)) break;
        }
      }
    }
    return hide_map;
  }

  isVisible([tx, ty]: Coord) {
    if (this.edge(tx) || this.edge(ty)) return true;
    const sight = this.sight_check([tx, ty]);
    return Object.keys(sight).length !== 4;
  }

  output() {
    const visible = this.grid.map((_, y) =>
      _.filter((_, x) => this.isVisible([x, y])).length
    ).reduce((a, b) => a + b);
    return visible;
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load("data/data.txt");
}
