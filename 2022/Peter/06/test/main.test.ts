// @jorjun  Vviii ☉ : ♐ ☽ : ♉

import { assertEquals } from "@deno/std/testing/asserts.ts";

import { Advent } from "../advent.ts";

Deno.test("Silver. Day 6: Tuning Trouble", async (tst) => {
  const silver = new Advent("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 4);
  await tst.step("Sample", async () => {
    assertEquals(silver.output(), 7);
  });

  await tst.step("Real", async () => {
    const silver = new Advent();
    await silver.load("data/data.txt");
    assertEquals(silver.output(), 1909);
  });
});

Deno.test("Gold. Day 5: Tuning Trouble", async (tst) => {
  await tst.step("Sample-1", async () => {
    const gold1 = new Advent("bvwbjplbgvbhsrlpgdmjqwftvncz");
    assertEquals(gold1.output(), 5);
  });

  await tst.step("Sample-2", async () => {
    const gold2 = new Advent("nppdvjthqldpwncqszvftbrmjlhg", 14);
    assertEquals(gold2.output(), 23);
  });

  await tst.step("Real", async () => {
    const gold = new Advent("", 14);
    await gold.load("data/data.txt");
    assertEquals(gold.output(), 3380);
  });
});
