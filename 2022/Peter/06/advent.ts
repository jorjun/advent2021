// @jorjun  Vviii ☉ : ♐ ☽ : ♉
// --- Day 6: Tuning Trouble ---

export class Advent {
  constructor(private code = "", private msg_len = 4) {}

  async load(path = "data/data.txt") {
    this.code = await Deno.readTextFile(path);
  }

  get_slice = (data: string[], ix: number, count = this.msg_len) =>
    data.slice(ix, Math.min(ix + count, data.length));

  output() {
    const source = this.code.split("");
    for (let ix = 0; ix < this.code.length - this.msg_len; ix++) {
      const buffer = this.get_slice(source, ix),
        unique = buffer.reduce((
          prv,
          cur,
        ) => prv.includes(cur) ? prv : prv + cur);
      if (unique.length == this.msg_len) {
        return ix + this.msg_len;
      }
    }
  }
}

// Main only
if (import.meta.main) {
  const q = new Advent();
  await q.load();
  console.log(q.output());
}
