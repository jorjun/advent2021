# Advent of Code 2021 [Typescript]


## Nerd assistance:

.vscode folder contains setup to enable debugging

from command line, eg:
```
deno run -A q1.ts
```

## Description
Showcasing elegant literate style. 
Layer your code so that 'What' is in a separate layer to 'How'.

Typescript has flaws, but generally it is good for a wide range of purposes, there is no good reason to continue using javascript. 
And Python's support for types is not as mature. 
Make the switch!

Do not forgo typing - they help others to understand your code, and the IDE to help you.

Programming is essentially a form of communication - with humans not machines.


## Installation
```
brew install deno
```

## License

Private, do not share or publish
